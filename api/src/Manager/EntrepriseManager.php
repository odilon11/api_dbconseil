<?php

namespace App\Manager;

use App\Entity\Api\ApiResponse;
use App\Services\ApiRequest;
use Doctrine\DBAL\Driver\AbstractDriverException;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EntrepriseManager extends BaseManager
{
    const SERVICE_NAME = 'app.entreprise_manager';

    public function __construct(EntityManagerInterface $entityManager, $class)
    {
        parent::__construct($entityManager, $class);
    }

    /**
     * Create object entrepriseManager
     * @param $request
     * @return mixed
     */
    public function createEntreprise(){
        $entreprise = $this->createNew();

        return $entreprise;
    }

    /**
     * Get all Office
     * @return array
     */
    public function getAll(){
        $responseApi = new ApiResponse();
        $data = [];
        $requestApi = new ApiRequest();
        $id = $requestApi->getBodyRawParam("id");
        $filter["id"] = $id;
        $filter["skype"] = $requestApi->getBodyRawParam("skype");
        $offices = $this->repository->getAllByFilter($filter);
        foreach ($offices as $ff){
            $data[] = $ff->getData();
        }
        $responseApi->setCode(Response::HTTP_OK);
        $responseApi->setMessage('All data');
        $responseApi->setData($data);

        return new JsonResponse($responseApi, Response::HTTP_OK);
    }
    /**
     * Create Office
     * @return array
     */
    public function add($user){
        $responseApi = new ApiResponse();
        $requestApi = new ApiRequest();
        if(!$requestApi->getBodyRawParam("name") || !$requestApi->getBodyRawParam("email")){
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        if ($this->findOneBy(["user" => $user,"nomCommercial" => $requestApi->getBodyRawParam("name")])) {
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Office already exist.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        $office = $this->createOffice();
        try{
            $office->setPersoMorale($requestApi->getBodyRawParam("perso_morale"));
            $office->setSiret1($requestApi->getBodyRawParam("siret1"));
            $office->setSiret2($requestApi->getBodyRawParam("siret2"));
            $office->setNomCommercial($requestApi->getBodyRawParam("trade_name"));
            $office->setTitre($requestApi->getBodyRawParam("title"));
            $office->setNom($requestApi->getBodyRawParam("name"));
            $office->setPrenom($requestApi->getBodyRawParam("first_name"));
            $office->setAdresse1($requestApi->getBodyRawParam("address1"));
            $office->setAdresse2($requestApi->getBodyRawParam("address2"));
            $office->setCp($requestApi->getBodyRawParam("cp"));
            $office->setVille($requestApi->getBodyRawParam("city"));
            $office->setPays($requestApi->getBodyRawParam("country"));
            $office->setEmail($requestApi->getBodyRawParam("email"));
            $office->setTelephone($requestApi->getBodyRawParam("tel"));
            $office->setSite($requestApi->getBodyRawParam("web_site"));
            $office->setImage($requestApi->getBodyRawParam("image"));
            $office->setIdInfoBanque($requestApi->getBodyRawParam("infos_bank"));
            $office->setUser($user);
            $this->saveAndFlush($office);

            $responseApi->setMessage('Office created.');
            $responseApi->setData($office->getData());

            return new JsonResponse($responseApi, Response::HTTP_OK);
        }
        catch (Exception $e){
            die("Error");
        }
    }

    /**
     * Modify Office
     * @return array
     */
    public function edit($user){
        $responseApi = new ApiResponse();
        $requestApi = new ApiRequest();
        if(!$requestApi->getBodyRawParam("name") || !$requestApi->getBodyRawParam("id")){
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        $office = $this->find($requestApi->getBodyRawParam("id"));
        if(!$office){
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('Office not found.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }
        if($user != $office->getUser()){
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('User haven\'t permission to edit.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }

        if ($this->findOneBy(["user" => $user,"nomCommercial" => $requestApi->getBodyRawParam("name")])) {
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Office existed.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        try{
            $office->setPersoMorale($requestApi->getBodyRawParam("perso_morale"));
            $office->setSiret1($requestApi->getBodyRawParam("siret1"));
            $office->setSiret2($requestApi->getBodyRawParam("siret2"));
            $office->setNomCommercial($requestApi->getBodyRawParam("trade_name"));
            $office->setTitre($requestApi->getBodyRawParam("title"));
            $office->setNom($requestApi->getBodyRawParam("name"));
            $office->setPrenom($requestApi->getBodyRawParam("first_name"));
            $office->setAdresse1($requestApi->getBodyRawParam("address1"));
            $office->setAdresse2($requestApi->getBodyRawParam("address2"));
            $office->setCp($requestApi->getBodyRawParam("cp"));
            $office->setVille($requestApi->getBodyRawParam("city"));
            $office->setPays($requestApi->getBodyRawParam("country"));
            $office->setEmail($requestApi->getBodyRawParam("email"));
            $office->setTelephone($requestApi->getBodyRawParam("tel"));
            $office->setSite($requestApi->getBodyRawParam("web_site"));
            $office->setImage($requestApi->getBodyRawParam("image"));
            $office->setIdInfoBanque($requestApi->getBodyRawParam("infos_bank"));
            $office->setUser($user);
            $this->saveAndFlush($office);

            $responseApi->setMessage('Office edited.');
            $responseApi->setData($office->getData());

            return new JsonResponse($responseApi, Response::HTTP_OK);
        }
        catch (Exception $e){
            die("Error");
        }
    }
    /**
     * Delete Office
     * @return array
     */
    public function deleteOffice($user, $id)
    {
        $responseApi = new ApiResponse();
        if (!$id) {
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        $office = $this->find($id);
        if (!$office) {
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('Office not found.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }
        if ($user != $office->getUser()) {
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('User haven\'t permission to edit.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }
        $this->delete($office);
        $this->saveAndFlush($office);
        return new JsonResponse($responseApi, Response::HTTP_OK);
    }
}
