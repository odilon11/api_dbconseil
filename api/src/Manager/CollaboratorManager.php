<?php

namespace App\Manager;

use App\Entity\Api\ApiResponse;
use App\Entity\Constants\Constant;
use App\Entity\User;
use App\Services\ApiRequest;
use App\Entity\Collaborateur;
use Doctrine\DBAL\Driver\AbstractDriverException;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CollaboratorManager extends BaseManager
{
    const SERVICE_NAME = 'app.collaborator_manager';

    public function __construct(EntityManagerInterface $entityManager, $class)
    {
        parent::__construct($entityManager, $class);
    }

    /**
     * Create object CollaboratorManager
     * @param $request
     * @return mixed
     */
    public function createCollabo(){
        $collabo = $this->createNew();

        return $collabo;
    }

    /**
     * Get all Office
     * @return array
     */
    public function getAll(){
        $responseApi = new ApiResponse();
        $data = [];
        $requestApi = new ApiRequest();
        $id = $requestApi->getBodyRawParam("id");
        $filter["id"] = $id;
        $filter["skype"] = $requestApi->getBodyRawParam("skype");
        $offices = $this->repository->getAllByFilter($filter);
        foreach ($offices as $ff){
            $data[] = $ff->getData();
        }
        $responseApi->setCode(Response::HTTP_OK);
        $responseApi->setMessage('All data');
        $responseApi->setData($data);

        return new JsonResponse($responseApi, Response::HTTP_OK);
    }
    /**
     * Create Office
     * @return array
     */
    public function add($user){
        $responseApi = new ApiResponse();
        $requestApi = new ApiRequest();
        if(!$requestApi->getBodyRawParam("username") || !$requestApi->getBodyRawParam("email")){
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        try{
            if(!$requestApi->getBodyRawParam("idUser") && $this->entityManager->getRepository("App:User")
                    ->findBy(["email"=>$requestApi->getBodyRawParam("email")])){
                $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Email already used.');
                return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
            }
            if(!$requestApi->getBodyRawParam("idUser") && $this->entityManager->getRepository("App:User")
                    ->findBy(["email"=>$requestApi->getBodyRawParam("username")])){
                $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Username already used.');
                return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
            }
            if($requestApi->getBodyRawParam("idUser")) {
                $newUser = $this->entityManager->getRepository("App:User")
                    ->find($requestApi->getBodyRawParam("idUser"));
                if (!$newUser) {
                    $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('User not found.');
                    return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
                }
            }
            else{
                $newUser = new User();
                $newUser->setEmail($requestApi->getBodyRawParam("email"));
                $newUser->setUsername($requestApi->getBodyRawParam("username"));
            }
            $newUser->setPlainPassword(md5(uniqid()));
            $newUser->setConfirmationToken(md5(uniqid()));
            $type = $this->entityManager->getRepository("App:Type")
                ->find(Constant::TYPE_COLLABORATOR);
            $newUser->setType($type);
            $newUser->setNom($requestApi->getBodyRawParam("username"));
            $newUser->setPrenom($requestApi->getBodyRawParam("firstname"));
            $newUser->setUtilisateur($requestApi->getBodyRawParam("username").' 
            '.$requestApi->getBodyRawParam("firstname"));
            $user->setTel($requestApi->getBodyRawParam("tel"));
            $this->saveAndFlush($newUser);

            $collab = new Collaborateur();
            $collab->setIdUser($user->getId());
            $collab->setSkype($requestApi->getBodyRawParam("skype"));
            $office = $this->entityManager->getRepository("App:Bureau")
                ->find($requestApi->getBodyRawParam("idBureau"));
            $collab->setBureau($office);
            $collab->setImage($requestApi->getBodyRawParam("image"));
            $this->saveAndFlush($collab);

            $responseApi->setMessage('Collaborator created.');
            $responseApi->setData($collab->getData());

            return new JsonResponse($responseApi, Response::HTTP_OK);
        }
        catch (Exception $e){
            die("Error");
        }
    }

    /**
     * Modify Office
     * @return array
     */
    public function edit($user){
        $responseApi = new ApiResponse();
        $requestApi = new ApiRequest();
        if(!$requestApi->getBodyRawParam("name") || !$requestApi->getBodyRawParam("id")){
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        $office = $this->find($requestApi->getBodyRawParam("id"));
        if(!$office){
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('Office not found.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }
        if($user != $office->getUser()){
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('User haven\'t permission to edit.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }

        if ($this->findOneBy(["user" => $user,"nomCommercial" => $requestApi->getBodyRawParam("name")])) {
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Office existed.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        try{
            $office->setPersoMorale($requestApi->getBodyRawParam("perso_morale"));
            $office->setSiret1($requestApi->getBodyRawParam("siret1"));
            $office->setSiret2($requestApi->getBodyRawParam("siret2"));
            $office->setNomCommercial($requestApi->getBodyRawParam("trade_name"));
            $office->setTitre($requestApi->getBodyRawParam("title"));
            $office->setNom($requestApi->getBodyRawParam("name"));
            $office->setPrenom($requestApi->getBodyRawParam("first_name"));
            $office->setAdresse1($requestApi->getBodyRawParam("address1"));
            $office->setAdresse2($requestApi->getBodyRawParam("address2"));
            $office->setCp($requestApi->getBodyRawParam("cp"));
            $office->setVille($requestApi->getBodyRawParam("city"));
            $office->setPays($requestApi->getBodyRawParam("country"));
            $office->setEmail($requestApi->getBodyRawParam("email"));
            $office->setTelephone($requestApi->getBodyRawParam("tel"));
            $office->setSite($requestApi->getBodyRawParam("web_site"));
            $office->setImage($requestApi->getBodyRawParam("image"));
            $office->setIdInfoBanque($requestApi->getBodyRawParam("infos_bank"));
            $office->setUser($user);
            $this->saveAndFlush($office);

            $responseApi->setMessage('Office edited.');
            $responseApi->setData($office->getData());

            return new JsonResponse($responseApi, Response::HTTP_OK);
        }
        catch (Exception $e){
            die("Error");
        }
    }
    /**
     * Delete Collabor
     * @return array
     */
    public function deleteColab($user, $id)
    {
        $responseApi = new ApiResponse();
        if (!$id) {
            $responseApi->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Input not valid.');
            return new JsonResponse($responseApi, Response::HTTP_BAD_REQUEST);
        }
        $collab = $this->find($id);
        if (!$collab) {
            $responseApi->setCode(Response::HTTP_NOT_FOUND)->setMessage('Collaborator not found.');
            return new JsonResponse($responseApi, Response::HTTP_NOT_FOUND);
        }
        $this->delete($collab);
        $this->saveAndFlush($collab);
        return new JsonResponse($responseApi, Response::HTTP_OK);
    }
}
