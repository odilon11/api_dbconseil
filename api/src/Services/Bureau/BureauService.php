<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Bureau;


use App\Entity\Bureau;
use App\Entity\Cabinet;
use App\Entity\Collaborateur;
use App\Entity\DetExpert;
use App\Entity\Entites;
use App\Entity\Entreprise;
use App\Entity\ListeBureau;
use App\Services\ToolsService;
use Symfony\Component\Validator\Constraints\Date;
use Doctrine\ORM\EntityManager;

class BureauService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;


    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Bureau';
    }

    public function addBureau($data){

        if($this->state['code']  == 200){
            $status = $this->tools->isExist($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']],'nom commercial');
            if($status['code'] == 401){
                return $status;
            }else{
                return $this->updateBureau($data,'add');
            }
        }else{
            return $this->state;
        }
    }

    public function editBureau($data){
        if($this->state['code'] == 200){
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
           if($bureau->getNomCommercial() != $data['nomCommercial']) {
               $status = $this->tools->isExist($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']],'nom commercial');
               if($status['code'] == 401){
                   return $status;
               }else{
                   return $this->updateBureau($data,'edit');
               }
           }else{
               return $this->updateBureau($data,'edit');
           }

        }else{
            return $this->state;
        }
    }

    public function updateBureau($data, $mode){


        if($mode == 'add'){
            $bureau = new Bureau();
        }else{
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
        }

            $bureau->setNomCommercial($data['nomCommercial']);
            if(isset($data['persoMorale'])){
                $bureau->setPersoMorale($data['persoMorale']);
            }
            if(isset($data['siret1'])){
                $bureau->setSiret1($data['siret1']);
            }
            if(isset($data['siret2'])){
                $bureau->setSiret2($data['siret2']);
            }
            if(isset($data['titre'])){
                $bureau->setTitre($data['titre']);
            }
            if(isset($data['nom'])){
                $bureau->setNom($data['nom']);
            }
            if(isset($data['prenom'])){
                $bureau->setPrenom($data['prenom']);
            }
            if(isset($data['adresse1'])){
                $bureau->setAdresse1($data['adresse1']);
            }
            if(isset($data['adresse2'])){
                $bureau->setAdresse2($data['adresse2']);
            }
            if(isset($data['cp'])){
                $bureau->setCp($data['cp']);
            }
            if(isset($data['ville'])){
                $bureau->setVille($data['ville']);
            }
            if(isset($data['pays'])){
                $bureau->setPays($data['pays']);
            }
            if(isset($data['email'])){
                $bureau->setEmail($data['email']);
            }
            if(isset($data['tel'])){
                $bureau->setTelephone($data['tel']);
            }
            if(isset($data['site'])){
                $bureau->setSite($data['site']);
            }
            if(isset($data['image'])){
                $bureau->setImage($data['image']);
            }
            if(isset($data['idInfoBanque'])){
                $bureau->setIdInfoBanque($data['idInfoBanque']);
            }

            $cp = isset($data['cp'])?$data['cp']:'';
            $ville = isset($data['ville'])?$data['ville']:'';
            $nom = isset($data['nom'])?$data['nom']:'';

            if($mode == 'add'){
                $bureau->setUser($this->user);
                $this->em->persist($bureau);
                $this->em->flush();

                $paiement = $this->tools->getOneEntity('Entites',$data['idInfoBanque']);
                $libelle = ($paiement)?$paiement->getLibelle():"";
                $this->updateListeBureau($mode,$nom,$data['nomCommercial'],
                    $cp.' - '.$ville,$libelle,0,0,$this->user,$bureau);

                return $this->tools->getStateSession(200, 'Bureau ajouté');
            }else{
                $this->em->flush();
                $paiement = $this->tools->getOneEntity('Entites',$data['idInfoBanque']);

                $this->updateListeBureau($mode,$nom,$data['nomCommercial'],
                    $cp.' - '.$ville,$paiement->getLibelle(),0,0,$this->user,$bureau);

                return $this->tools->getStateSession(200, 'Bureau modifié');
            }

    }

    public function updateListeBureau($mode,$nom,$nomCom,$cpVille,$paiement,$nbColl,$nbClient,$user,$bureau){

        if($mode == 'add'){
            $listeBureau = new ListeBureau();
            $listeBureau->setUser($user);
            $listeBureau->setBureau($bureau);
        }else{
            $listeBureau = $this->tools->getOneEntityBy('ListeBureau',['bureau'=>$bureau]);

        }

        $listeBureau->setNomCommercial($nomCom);
        $listeBureau->setNom($nom);
        $listeBureau->setCpVille($cpVille);
        $listeBureau->setPaiement($paiement);
        $listeBureau->setNbCollab($nbColl);
        $listeBureau->setNbClient($nbClient);

        if($mode == 'add'){
            $this->em->persist($listeBureau);
            $this->em->flush();
        }else{
            $this->em->flush();
        }

    }

    public function deleteBureau($data){

        if($this->state['code'] == 200){
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
            $listeBureau = $this->tools->getOneEntityBy('ListeBureau',['bureau'=>$bureau]);
            if($bureau){
                $libelle = $bureau->getNomCommercial();
                $this->em->remove($bureau);
                $this->em->remove($listeBureau);
                $this->em->flush();
                return $this->tools->getStateSession(200, $libelle.' supprimé');
            }else{
                return $this->tools->getStateSession(401, 'Element non trouvé');
            }

        }else{
            return $this->state;
        }

    }

    public function getOfficeByFilter($data,$user){
        $office = $this->em->getRepository("App:ListeBureau")->getAllByFilter($data,$user);
        return $office;
    }

    public function getAllBureau($data){

        if($this->state['code'] == 200){
            //$bureaux = $this->tools->getAllEntities($this->entity,['user'=>$this->user]);
            $bureaux = $this->getOfficeByFilter($data,$this->user);

            if(count($bureaux) == 0){
                return $this->tools->getStateSession(401, 'Pas de bureau');
            }else{

                $listeBureau = [];
                $totalCollab = 0;
                $totalClient = 0;
                foreach($bureaux as $bureau){
                    $totalCollab += $bureau->getNbCollab();
                    $totalClient += $bureau->getNbClient();
                }

                foreach($bureaux as $bureau){

                    $listeBureau[] = [
                        'id'=> $bureau->getBureau()->getId(),
                        'nom'=> $bureau->getNom(),
                        'nomCommercial'=> $bureau->getNomCommercial(),
                        'cpVille'=> $bureau->getCpVille(),
                        'paiement'=> $bureau->getPaiement(),
                        'nbCollab'=> $bureau->getNbCollab(),
                        'nbClient'=> $bureau->getNbClient(),
                        'totalCollab'=> $totalCollab,
                        'totalClient'=> $totalClient
                    ];
                }
                return $listeBureau;
            }

        }else{
            return $this->state;
        }
    }

    public function getDataBureau($data){

        if($this->state['code'] == 200){
            $bureaux = $this->tools->getEntities($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']]);
            if($bureaux){
                return $bureaux;
            }else{
                return $this->tools->getStateSession(401, 'Pas de bureau');
            }
        }else{
            return $this->state;
        }
    }

    public function getOneBureauById($data){

        if($this->state['code'] == 200){
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
            if($bureau){
                return $bureau;
            }else{
                return $this->tools->getStateSession(401, 'Pas de bureau');
            }
        }else{
            return $this->state;
        }

    }

}