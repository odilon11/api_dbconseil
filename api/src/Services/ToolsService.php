<?php
/**
 * Created by PhpStorm.
 * User: JO
 * Date: 03/04/2018
 * Time: 18:07
 */

namespace App\Services;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface; // Utilisation de la vue de FOSRestBundle
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

class ToolsService
{

    public $container;
    public $tokenStorage;
    public $em;

    public function __construct(ContainerInterface $container,EntityManager $em, TokenStorageInterface $tokenStorage)
    {
        $this->container = $container;
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function getDataJson($request){
        $data = $request->getContent();
        $tab = json_decode($data, true);

        return $tab;
    }

    public function sendDataJson($data){
        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function sendResponseJson($data){
        return new JsonResponse($data);
    }


    public function getUserDirectory($companyName){

        $directory = __DIR__.'/../../../'.$this->container->getParameter('user_directory') . '/'.$companyName. '/';
        return $directory;
    }

    public function sendMailRegister($data,$token,$subject,$type)
    {
        //        $mailFrom = $this->container->getParameter('mailer_user');
        $mailFrom = $this->container->getParameter('no_reply_address');
        $mailTo = $data['email'];
        $prenom = isset($data['prenom']) ? $data['prenom']: '';
        $tamplate = ':email:registerEmail.html.twig';

        $param    = array(
            'subject'  => $subject,
            'mailto' => $mailTo,
            'mailfrom' => $mailFrom,
            'nom' => $prenom,
            'token' => $token,
            'type' => $type
        );

        $body = $this->container->get('templating')->render($tamplate,$param);
        $this->container->get('app.mailer')->sendMail($mailFrom,$mailTo,$subject,$body);
    }

    public function getStateSession($code, $message){

        return [
            'code'=>$code,
            'message'=>$message
        ];
    }
    public function getUser(){

        return $this->tokenStorage->getToken()->getUser();
    }

    public function getStatusByToken(){

        $user = $this->getUser();

       // if($user->getToken() != $this->tokenStorage->getToken()->getCredentials()){
        if(!$user){
             return $this->getStateSession(401, 'Session expirée!');
        }else{
            return $this->getStateSession(200, 'Session en cours');
        }
    }

    public function isExist($entity,$tab,$lib=null){

        $entite = $this->em->getRepository('App:'.$entity)->findOneBy($tab);
        if($entite){
            return $this->getStateSession(401, $lib.' Existe déjà ');
        }else{
            return $this->getStateSession(200, 'N\'existe pas ');
        }

    }

    public function getAllEntities($entity,$tab){

        return $this->em->getRepository('App:'.$entity)->findBy($tab);
    }

    public function getOneEntity($entity, $id){
        return $this->em->getRepository('App:'.$entity)->find($id);
    }

    public function getOneEntityBy($entity, $tab){
        return $this->em->getRepository('App:'.$entity)->findOneBy($tab);
    }

    public function updateUser($mode,$data,$type,$enable,$role,$idExpert=0){

        $userManager = $this->container->get('fos_user.user_manager');
        if($mode == 'add'){
            $user = $userManager->createUser();
            $user->setEmail($data['email']);
            $user->setIdExpert($idExpert);
            $user->setPlainPassword(md5(uniqid()));
            $user->setConfirmationToken(md5(uniqid()));
            $user->addRole($role);
            $user->setType($type);
        }else{
            $user = $userManager->findUserByEmail($data['email']);
        }

        $user->setNom($data['nom']);
        $user->setPrenom($data['prenom']);
        $user->setUtilisateur($data['nom'].' '.$data['prenom']);

        $user->setUsername($data['prenom']);
        $date = new \DateTime();
        if($user->getDateCreate() == ''){
            $user->setDateCreate($date->format('d-m-Y'));
        }
        $user->setDateActive($date->format('d-m-Y'));
        if(isset($data['tel'])){
            $user->setTel($data['tel']);
        }

        if(isset($data['adresse'])){
            $user->setAdresse($data['adresse']);
        }
        if(isset($data['validite'])){
            $user->setValidite($data['validite']);
        }
        if(isset($data['idFonction'])){
            $user->setIdFonction($data['idFonction']);
        }
        if(isset($data['idProfil'])){
            $user->setIdProfil($data['idProfil']);
        }
        if(isset($data['idBureau'])){
            $user->setIdBureau($data['idBureau']);
        }

        $user->setEnabled($enable);


        $userManager->updateUser($user);


        return $user;

    }



}