<?php
/**
 * Created by PhpStorm.
 * User: JOCELYN
 * Date: 27/09/2017
 * Time: 13:56
 */

namespace App\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use XLSXWriter;

class TestService
{

    private $container;

    public function __construct(ContainerInterface $container )
    {
        $this->container = $container;
    }

    public function generateXls()
    {
        $xlsfile = __DIR__.'/../../../web/generate/excel/test.xlsx';

        $header = array(
            'created'=>'date',
            'product_id'=>'integer',
            'quantity'=>'#,##0',
            'amount'=>'price',
            'description'=>'string',
            'tax'=>'[$$-1009]#,##0.00;[RED]-[$$-1009]#,##0.00',
        );
        $data = array(
            array('2015-01-01',873,1,'44.00','misc','=D2*0.05'),
            array('2015-01-12',324,2,'88.00','none','=D3*0.05'),
        );

        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Sheet1', $header,['fill' => '#dcdcdc'] );
        foreach($data as $row)
            $writer->writeSheetRow('Sheet1', $row );
        $writer->writeToFile($xlsfile);
    }


    public function generatePdf($name)
    {
        $pdfFile = __DIR__.'/../../../web/generate/pdf/test.pdf';

        $pdf = $this->container->get('knp_snappy.pdf');
        $pdf->generateFromHtml(
            $this->container->get('templating')->render('@App/Default/index.html.twig',[], true),
            $pdfFile
        );
        $pdf->getInternalGenerator()->setTimeout(10000);
    }

}