<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Entreprise;


use App\Entity\Entreprise;
use App\Services\ToolsService;
use Doctrine\ORM\EntityManager;

class EntrepriseService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;


    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Entreprise';
    }

    private function updateEntreprise($data,$mode){

            if($mode == 'add'){
                $entreprise = new Entreprise();
            }else{
                $entreprise = $this->tools->getOneEntity($this->entity,$data['id']);
            }

            $entreprise->setPersoMorale($data['personneMorale']);
            $entreprise->setSiret($data['siret']);
            $entreprise->setSiret1($data['siret1']);
            $entreprise->setNomCommercial($data['nomCommercial']);
            $entreprise->setNom($data['nom']);
            $entreprise->setPrenom($data['prenom']);
            $entreprise->setResponsable($data['nom'].' '.$data['prenom']);
            $entreprise->setAdresse1($data['adresse1']);
            $entreprise->setAdresse2($data['adresse2']);
            $entreprise->setCp($data['cp']);
            $entreprise->setVille($data['ville']);
            $entreprise->setPays($data['pays']);
            $entreprise->setEmail($data['email']);
            $entreprise->setTel($data['tel']);
            $entreprise->setSite($data['site']);
            $entreprise->setActivite1($data['activite1']);
            $entreprise->setActivite2($data['activite2']);
            $entreprise->setCapital($data['capital']);
            $entreprise->setSocial($data['social']);
            $entreprise->setGreffe($data['greffe']);
            $entreprise->setIdGroupe($data['idGroupe']);
            $bureau = $this->tools->getOneEntity('Bureau',$data['idBureau']);
            $entreprise->setBureau($bureau);
            $entreprise->setIdRegimeFiscal($data['idRegimeFiscal']);
            $entreprise->setIdImposition($data['idImposition']);
            $entreprise->setIdRegimeTva($data['idRegimeTva']);
            $entreprise->setIdTypeTva($data['idTypeTva']);
            $entreprise->setNumTvaInfra1($data['numTvaInfra1']);
            $entreprise->setNumTvaInfra2($data['numTvaInfra2']);
            $entreprise->setNumTvaInfra3($data['numTvaInfra3']);
            $entreprise->setIdTypeCompta($data['idTypeCompta']);
            $entreprise->setIdInfoBanque($data['idInfoBanque']);
            $entreprise->setActiverTous($data['activerTous']);
            $entreprise->setVentes($data['ventes']);
            $entreprise->setAchats($data['achats']);
            $entreprise->setNdf($data['ndf']);
            $entreprise->setCaisse($data['caisse']);
            $entreprise->setBanque($data['banque']);
            $entreprise->setTabBord($data['tabBord']);
            $entreprise->setComptabilte($data['comptabilte']);
            $entreprise->setGed($data['ged']);
            $entreprise->setAffectation1($data['affectation1']);
            $entreprise->setAffectation2($data['affectation2']);
            $entreprise->setAffectation3($data['affectation3']);
            $entreprise->setAffectation4($data['affectation4']);
            $entreprise->setAffectation5($data['affectation5']);
            $entreprise->setAffectationCab($data['affectationCab']);

            if($mode == 'add'){
                $entreprise->setUser($this->user);
                $this->em->persist($entreprise);
                $this->em->flush();
                return $this->tools->getStateSession(200, 'Entreprise ajouté');
            }else{
                $this->em->flush();
                return $this->tools->getStateSession(200, 'Entreprise modifié');
            }

    }


    public function addEntreprise($data){

        if($this->state['code']  == 200){
            $status = $this->tools->isExist($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']]);
            if($status['code'] == 401){
                return $status;
            }else{
                return $this->updateEntreprise($data,'add');
            }
        }else{
            return $this->state;
        }

    }

    public function editEntreprise($data){

        if($this->state['code']  == 200){
            return $this->updateEntreprise($data,'edit');
        }else{
            return $this->state;
        }

    }

    public function getOneEntrepriseById($data){

        if($this->state['code'] == 200){

            $entreprise = $this->tools->getOneEntity($this->entity,$data['id']);
            if($entreprise){
                $Entreprise= [
                    "id"=>$entreprise->getId(),
                    "siret"=>$entreprise->getSiret(),
                    "nomCommercial"=>$entreprise->getNomCommercial(),
                    "nom"=>$entreprise->getNom(),
                    "prenom"=>$entreprise->getPrenom(),
                    "responsable"=>$entreprise->getResponsable(),
                    "adresse1"=>$entreprise->getAdresse1(),
                    "adresse2"=>$entreprise->getAdresse2(),
                    "cp"=>$entreprise->getCp(),
                    "ville"=>$entreprise->getVille(),
                    "pays"=>$entreprise->getPays(),
                    "email"=>$entreprise->getEmail(),
                    "tel"=>$entreprise->getTel(),
                    "site"=>$entreprise->getSite(),
                    "activite1"=>$entreprise->getActivite1(),
                    "activite2"=>$entreprise->getActivite2(),
                    "social"=>$entreprise->getSocial(),
                    "capital"=>$entreprise->getCapital(),
                    "idGroupe"=>$entreprise->getIdGroupe(),
                    "idImposition"=>$entreprise->getIdImposition(),
                    "idRegimeTva"=>$entreprise->getIdRegimeTva(),
                    "idRegimeFiscal"=>$entreprise->getIdRegimeFiscal(),
                    "idTypeTva"=>$entreprise->getIdTypeTva(),
                    "numTvaInfra1"=>$entreprise->getNumTvaInfra1(),
                    "numTvaInfra2"=>$entreprise->getNumTvaInfra2(),
                    "numTvaInfra3"=>$entreprise->getNumTvaInfra3(),
                    "idTypeCompta"=>$entreprise->getIdTypeCompta(),
                    "idInfoBanque"=>$entreprise->getIdInfoBanque(),
                    "greffe"=>$entreprise->getGreffe(),
                    "activerTous"=>$entreprise->getActiverTous(),
                    "ventes"=>$entreprise->getVentes(),
                    "achats"=>$entreprise->getAchats(),
                    "ndf"=>$entreprise->getNdf(),
                    "caisse"=>$entreprise->getCaisse(),
                    "banque"=>$entreprise->getBanque(),
                    "tabBord"=>$entreprise->getTabBord(),
                    "comptabilte"=>$entreprise->getComptabilte(),
                    "ged"=>$entreprise->getGed(),
                    "affectation1"=>$entreprise->getAffectation1(),
                    "affectation2"=>$entreprise->getAffectation2(),
                    "affectation3"=>$entreprise->getAffectation3(),
                    "affectation4"=>$entreprise->getAffectation4(),
                    "affectation5"=>$entreprise->getAffectation5(),
                    "affectationCab"=>$entreprise->getAffectationCab(),
                    "persoMorale"=>$entreprise->getPersoMorale(),
                ];
                return $Entreprise;
            }else{
                return $this->tools->getStateSession(401, 'Pas d\'entreprises');
            }
        }else{
            return $this->state;
        }
    }

    public function getAllEntreprise($data){

        if($this->state['code'] == 200){

           // $entreprises = $this->tools->getAllEntities($this->entity,['user'=>$this->user]);
            $entreprises = $this->em->getRepository('App:Entreprise')->getAllByFilter($data,$this->user);
            if(count($entreprises) == 0){
                return $this->tools->getStateSession(401, 'Pas d\'entreprise');
            }else{
                $listeEntreprises = [];
                foreach($entreprises as $entreprise){
                    $listeEntreprises[] = [
                        "responsable"=>$entreprise->getResponsable(),
                        "nomCommercial"=>$entreprise->getNomCommercial(),
                        "cloture"=>$entreprise->getCloture(),
                        "actif"=>$entreprise->getActif(),
                        "actif"=>$entreprise->getActif(),
                        "ventes"=>$entreprise->getVentes(),
                        "achats"=>$entreprise->getAchats(),
                        "ndf"=>$entreprise->getNdf(),
                        "caisse"=>$entreprise->getCaisse(),
                        "banque"=>$entreprise->getBanque(),
                        "tabBord"=>$entreprise->getTabBord()
                    ];
                    /*$listeEntreprises[] = [
                        "id"=>$entreprise->getId(),
                        "siret"=>$entreprise->getSiret(),
                        "nomCommercial"=>$entreprise->getNomCommercial(),
                        "nom"=>$entreprise->getNom(),
                        "prenom"=>$entreprise->getPrenom(),
                        "responsable"=>$entreprise->getResponsable(),
                        "adresse1"=>$entreprise->getAdresse1(),
                        "adresse2"=>$entreprise->getAdresse2(),
                        "cp"=>$entreprise->getCp(),
                        "ville"=>$entreprise->getVille(),
                        "pays"=>$entreprise->getPays(),
                        "email"=>$entreprise->getEmail(),
                        "tel"=>$entreprise->getTel(),
                        "site"=>$entreprise->getSite(),
                        "activite1"=>$entreprise->getActivite1(),
                        "activite2"=>$entreprise->getActivite2(),
                        "social"=>$entreprise->getSocial(),
                        "capital"=>$entreprise->getCapital(),
                        "idGroupe"=>$entreprise->getIdGroupe(),
                        "idImposition"=>$entreprise->getIdImposition(),
                        "idRegimeTva"=>$entreprise->getIdRegimeTva(),
                        "idRegimeFiscal"=>$entreprise->getIdRegimeFiscal(),
                        "idTypeTva"=>$entreprise->getIdTypeTva(),
                        "numTvaInfra1"=>$entreprise->getNumTvaInfra1(),
                        "numTvaInfra2"=>$entreprise->getNumTvaInfra2(),
                        "numTvaInfra3"=>$entreprise->getNumTvaInfra3(),
                        "idTypeCompta"=>$entreprise->getIdTypeCompta(),
                        "idInfoBanque"=>$entreprise->getIdInfoBanque(),
                        "greffe"=>$entreprise->getGreffe(),
                        "activerTous"=>$entreprise->getActiverTous(),
                        "ventes"=>$entreprise->getVentes(),
                        "achats"=>$entreprise->getAchats(),
                        "ndf"=>$entreprise->getNdf(),
                        "caisse"=>$entreprise->getCaisse(),
                        "banque"=>$entreprise->getBanque(),
                        "tabBord"=>$entreprise->getTabBord(),
                        "comptabilte"=>$entreprise->getComptabilte(),
                        "ged"=>$entreprise->getGed(),
                        "affectation1"=>$entreprise->getAffectation1(),
                        "affectation2"=>$entreprise->getAffectation2(),
                        "affectation3"=>$entreprise->getAffectation3(),
                        "affectation4"=>$entreprise->getAffectation4(),
                        "affectation5"=>$entreprise->getAffectation5(),
                        "affectationCab"=>$entreprise->getAffectationCab(),
                        "persoMorale"=>$entreprise->getPersoMorale(),
                ];*/
                }


                return $listeEntreprises;
            }

        }else{
            return $this->state;
        }
    }


    public function deleteEntreprise($data){

        if($this->state['code'] == 200){
            $entreprise = $this->tools->getOneEntity($this->entity,$data['id']);
            if($entreprise){
                $this->em->remove($entreprise);
                $this->em->flush();
                return $this->tools->getStateSession(200, $entreprise->getNomCommercial().' supprimé');
            }else{
                return $this->tools->getStateSession(401, 'Element non trouvé');
            }

        }else{
            return $this->state;
        }

    }


    private function existBureau($data){

        $cabinet = $this->em->getRepository('App:Bureau')->findOneBy(['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']]);
        if($cabinet){
            return $this->tools->getStateSession(401, 'Le bureau '.$data['nomCommercial'].' existe déjà ');
        }else{
            return $this->tools->getStateSession(200, 'Le bureau n\'existe pas ');
        }

    }

}