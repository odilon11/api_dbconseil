<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Cabinet;


use App\Entity\Bureau;
use App\Entity\Cabinet;
use App\Entity\Collaborateur;
use App\Entity\DetExpert;
use App\Entity\Entites;
use App\Entity\Entreprise;
use App\Services\ContainerService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Date;

class CabinetService
{

    public $container;
    public $em;
    private $tokenStorage;

    public function __construct(ContainerService $containerService, TokenStorageInterface $tokenStorage )
    {
        $this->container = $containerService->container;
        $this->em = $containerService->em;
        $this->tokenStorage = $tokenStorage;
    }

    public function getDataCabinet($data){
        $user = $this->tokenStorage->getToken()->getUser();

        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){
            $cabinets = $this->em->getRepository('App:Cabinet')->findBy(['user'=>$view['user'],'nomCommercial'=>$data['nomCommercial']]);
            if($cabinets){
                return $cabinets;
            }else{
                return [
                  'response'=>'Pas de données',
                    'status'=> true
                ];
            }
        }else{
            return $view;
        }
    }

    public function listingCabinet($data){

        $user = $this->tokenStorage->getToken()->getUser();
        $cabinets = $this->em->getRepository('App:Cabinet')->findBy(['user'=>$user]);
        if($cabinets){
            return $cabinets;
        }else{
            return [
                'response'=>'Pas de données',
                'status'=> true
            ];
        }


//        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
//        if($view['status']){
//            $cabinets = $this->em->getRepository('App:Cabinet')->findBy(['user'=>$view['user']]);
//            return $cabinets;
//        }else{
//            return $view;
//        }
    }

    public function updateCabinet($data){
        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){
            $cabinet = $this->em->getRepository('App:Cabinet')->find($data['id']);

            $cabinet->setSiret($data['siret']);
            $cabinet->setNomCommercial($data['nomCommercial']);
            $cabinet->setNom($data['nom']);
            $cabinet->setPrenom($data['prenom']);
            $cabinet->setAdresse1($data['adresse1']);
            $cabinet->setCp($data['cp']);
            $cabinet->setVille($data['pays']);
            $cabinet->setSite($data['site']);
            $cabinet->setActivite($data['activite']);
            $cabinet->setCapital($data['capital']);
            $cabinet->setGreffe($data['greffe']);
            $cabinet->setFiscal($data['fiscal']);
            $cabinet->setModePaie($data['modePaie']);
            $cabinet->setCloture($data['cloture']);
            $cabinet->setActif($data['actif']);
            $cabinet->setVentes($data['ventes']);
            $cabinet->setAchats($data['achats']);
            $cabinet->setNdf($data['ndf']);
            $cabinet->setCaisse($data['caisse']);
            $cabinet->setBanque($data['banque']);
            $cabinet->setTabBord($data['tabBord']);
            $cabinet->setAffectation1($data['affectation1']);
            $cabinet->setAffectation2($data['affectation2']);
            $cabinet->setAffectation3($data['affectation3']);
            $cabinet->setAffectation4($data['affectation4']);
            $cabinet->setAffectation5($data['affectation5']);
            $cabinet->setAffectationCab($data['affectationCab']);
            $cabinet->setPersoMorale($data['persoMorale']);

            $this->em->flush();

                return [
                    'response'=>'Cabinet modifié',
                    'status'=>true
                ];
        }else{
            return $view;
        }
    }

    public function deleteCabinet($data){

        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){
            $cabinet = $this->em->getRepository('App:Cabinet')->find($data['id']);
            if($cabinet){
               // $this->em->
                $this->em->remove($cabinet);
                $this->em->flush();
                return [
                    'response'=> $cabinet->getNomCommercial().' supprimé',
                    'status'=>true
                ];
            }else{
                return [
                    'response'=>'Element non trouvé',
                    'status'=>false
                ];
            }

        }else{
            return $view;
        }

    }

    public function addCabinet($data){

        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){
            $res = $this->existCabinet($data,$view['user']);
            if($res['status']){
                return $res;
            }else{
                return $this->ajoutCabinet($data,$view['user']);
            }

        }else{
            return $view;
        }

    }

    private function ajoutCabinet($data,$userExpert){



        $cabinet = new Cabinet();
        $cabinet->setUser($userExpert);
        $cabinet->setSiret($data['siret']);
        $cabinet->setNomCommercial($data['nomCommercial']);
        $cabinet->setNom($data['nom']);
        $cabinet->setPrenom($data['prenom']);
        $cabinet->setAdresse1($data['adresse1']);
        $cabinet->setCp($data['cp']);
        $cabinet->setVille($data['pays']);
        $cabinet->setSite($data['site']);
        $cabinet->setActivite($data['activite']);
        $cabinet->setCapital($data['capital']);
        $cabinet->setGreffe($data['greffe']);
        $cabinet->setFiscal($data['fiscal']);
        $cabinet->setModePaie($data['modePaie']);
        $cabinet->setCloture($data['cloture']);
        $cabinet->setActif($data['actif']);
        $cabinet->setVentes($data['ventes']);
        $cabinet->setAchats($data['achats']);
        $cabinet->setNdf($data['ndf']);
        $cabinet->setCaisse($data['caisse']);
        $cabinet->setBanque($data['banque']);
        $cabinet->setTabBord($data['tabBord']);
        $cabinet->setAffectation1($data['affectation1']);
        $cabinet->setAffectation2($data['affectation2']);
        $cabinet->setAffectation3($data['affectation3']);
        $cabinet->setAffectation4($data['affectation4']);
        $cabinet->setAffectation5($data['affectation5']);
        $cabinet->setAffectationCab($data['affectationCab']);
        $cabinet->setPersoMorale($data['persoMorale']);

        $this->em->persist($cabinet);
        $this->em->flush();

        return [
            'response'=>'Cabinet ajouté',
            'status'=>true
        ];
    }

    private function existCabinet($data,$user){

        $cabinet = $this->em->getRepository('App:Cabinet')->findOneBy(['user'=>$user,'nomCommercial'=>$data['nomCommercial']]);
        if($cabinet){
            return  [
                'response'=>'Le cabinet '.$data['nomCommercial'].' existe déjà ',
                'status'=>true
            ];
        }

        return false;

    }

}