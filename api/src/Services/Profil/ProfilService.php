<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 6/4/18
 * Time: 10:37 AM
 */

namespace App\Services\Profil;


use App\Entity\Profil;
use App\Services\ToolsService;
use Doctrine\ORM\EntityManager;

class ProfilService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;

    public function __construct(EntityManager $em, ToolsService $tools)
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Profil';
    }

    public function updateProfil($data,$mode){

        if($this->state['code']  == 200){
            $state = $this->tools->isExist($this->entity,['user'=>$this->user,'libelle'=>$data['libelle']]);
            if($state['code'] == 401){
                return $this->tools->getStateSession(401,'Profil '.$data['libelle'].' Existant');
            }else{

                if($mode == 'add'){
                    $profil = new Profil();
                    $profil->setUser($this->user);
                }else{
                    $profil = $this->tools->getOneEntity($this->entity,$data['id']);
                }

                $profil->setLibelle($data['libelle']);
                $profil->setDescription($data['description']);

                if($mode == 'add'){
                    $this->em->persist($profil);
                    $this->em->flush();
                    $message = $profil->getLibelle().' ajouté';
                }else{
                    $this->em->flush();
                    $message = $profil->getLibelle().' modifié';
                }


                return $this->tools->getStateSession(200,$message);
            }

        }else{
            return $this->state;
        }
    }


    public function deleteProfil($data){

        if($this->state['code']  == 200){
            $profil = $this->tools->getOneEntity($this->entity,$data['id']);
            $libelle = $profil->getLibelle();
            $this->em->remove($profil);
            $this->em->flush();

            return $this->tools->getStateSession(200,$profil->getType().' '.$libelle.' supprimé');

        }else{
            return $this->state;
        }

    }

    public function getAllProfil(){
        if($this->state['code']  == 200){
            $profils = $this->tools->getAllEntities($this->entity,['user'=>$this->user]);
            if(count($profils) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $profils;
            }

        }else{
            return $this->state;
        }

    }

    public function getAllProfilById($data){
        if($this->state['code']  == 200){
            $profil = $this->tools->getOneEntity($this->entity,$data['id']);
            if(!$profil){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $profil;
            }
        }else{
            return $this->state;
        }

    }


}