<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Utilisateur;


use App\Entity\DetExpert;
use App\Services\ContainerService;
use App\Services\ToolsService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Date;

class UserService
{

    public $container;
    public $em;
    public $tokenStorage;
    public $tools;

    public function __construct(ContainerService $containerService,TokenStorageInterface $tokenStorage,
        ToolsService $tools)
    {
        $this->container = $containerService->container;
        $this->em = $containerService->em;
        $this->tokenStorage = $tokenStorage;
        $this->tools = $tools;
    }

    public function getUtilisateur($data ){


        if($this->testExistance('email',trim($data['email']))){
            $message = 'Email existant: Veuillez changer l\'adresse email';
            $token= null;
        }else{
            if($this->testExistance('prenom',trim($data['prenom']))){
                $message = 'Prénom existant: Veuillez changer le prénom';
                $token= null;
            }else{
                $user = $this->upDateUser($data);
                $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
                $token = $jwtManager->create($user);
                $user->setToken($token);
                $this->em->flush();

                $this->container->get('app.tools')->sendMailRegister($data,$token,'Création de compte','Creation');
                $message = 'Merci! : Un email de confirmation a été envoyé vers votre compte';
            }

        }

        $res = [
            'message'=> $message,
            'email'=>$data['email'],
            'token'=>$token
        ];

        return $res;

    }

    public function miseAjourUser($data){

    }

    public function listingUser($data){

        $type = trim($data['type']);
        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){

            $users = $this->em->getRepository('App:User')->findBy(['type'=>$type]);

            return $users;
        }else{

            return $view;
        }


    }

    public function validMdpByUser($data){

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(["token"=>$data['token']]);

        if($user){

            $user->setPlainPassword($data['password']);
            $user->setConfirmationToken(md5(uniqid()));
            $user->setToken(md5(uniqid()));

            if($data['create']){
                $directory = $this->container->get('app.tools')->getUserDirectory($user->getEmail());
                if (!is_dir($directory)) {
                    mkdir($directory, 0777,true);
                    mkdir($directory.'logo', 0777,true);
                    mkdir($directory.'images', 0777,true);
                    mkdir($directory.'articles', 0777,true);
                }
            }


            $userManager->updateUser($user);

            return $this->tools->getStateSession(401, 'Votre compte est maintenant actif.');
        }else{
            return $this->tools->getStateSession(401, 'Votre session de saisi de mot de passe est expirée.');

        }

    }


    public function validUser($data){

        $idExpert = trim($data['idExpert']);
        $view = $this->checkLogin($data);

        if($view['status']){
            $user = $this->em->getRepository('App:User')->findOneBy(['id'=>$idExpert]);
            $user->setEnabled($data['enable']);
            $date = new \DateTime();
            if($user->getDateCreate() == ''){
                $user->setDateCreate($date->format('d-m-Y'));
            }
            $user->setDateActive($date->format('d-m-Y'));
            $this->em->flush();

            return $user;
        }else{
            return $view;
        }
    }



    public function updateExpert($data){

        $view = $this->container->get('app.tools')->checkLoginByToken($data['token']);
        if($view['status']){
            $detExpert = $this->em->getRepository('App:Expert')->findOneBy(['user'=>$view['user']]);

            if($detExpert){
                $detExpert->setSiret($data['siret']);
                $detExpert->setAdresse1($data['adresse1']);
                $detExpert->setAdresse2($data['adresse2']);
                $detExpert->setCp($data['cp']);
                $detExpert->setVille($data['ville']);
                $detExpert->setPays($data['pays']);
                $detExpert->setSite($data['site']);
                $this->em->flush();
            }else{
                $detExpert = new Expert();
                $detExpert->setUser($view['user']);
                $detExpert->setSiret($data['siret']);
                $detExpert->setAdresse1($data['adresse1']);
                $detExpert->setAdresse2($data['adresse2']);
                $detExpert->setCp($data['cp']);
                $detExpert->setVille($data['ville']);
                $detExpert->setPays($data['pays']);
                $detExpert->setSite($data['site']);
                $this->em->persist($detExpert);
                $this->em->flush();
            }

            return [
                'response'=>'Données expert comptable modifiées',
                'status'=>true
            ];

        }else{
            return $view;
        }
    }



    private function checkLogin($data){

        $username = trim($data['username']);
        $password = trim($data['password']);

        if($username != ''){
            $userManager = $this->container->get('fos_user.user_manager');

            $user = $userManager->findUserByUsernameOrEmail($username);
            if($user == null){
                $view = [
                    'response'=>'Nom d\'utilisateur incorrect',
                    'status'=>false
                ];

                return $view;
            }

            $isValid = $this->container->get('security.password_encoder')
                ->isPasswordValid($user, $password);

            if ($isValid) {
                if($user->isEnabled() == 1){
                    $response = 'OK';
                    $status = true;
                }else{
                    $response = 'Votre compte est expirée !';
                    $status = false;
                }

                $view = [
                    'response'=>$response,
                    'token'=>$user->getConfirmationToken(),
                    'status'=>$status
                ];

            }else{
                $view = [
                    'response'=>'Mot de passe incorrect',
                    'status'=>false
                ];
            }


        }else{
            $view = [
                'response'=>'Remplir le nom d\'utilisateur',
                'status'=>false
            ];
        }

        return $view;
    }

    public function loginUser($data){

        return $this->checkLogin($data);

    }

    public function setConfirmationToken(){
        $user = $this->tokenStorage->getToken()->getUser();
        $user->setToken($this->tokenStorage->getToken()->getCredentials());
        $this->em->flush();

        return [
            'code'=> 200,
            'message'=> 'Session démarée'
        ];
    }

    public function logoutUser(){

        $state = $this->tools->getStatusByToken();
        if($state['code'] == 200){
            $user = $this->tokenStorage->getToken()->getUser();
            $user->setToken(md5(uniqid()));
            $this->em->flush();
        }

        return [
            'code'=>401,
            'message'=>'Session expirée!'
        ];

    }

    public function resetPwd($data){

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['email'=>$data['email']]);
        if($user){
            $token = $user->getToken();
            if($token == '' || $token == null){
                $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
                $token = $jwtManager->create($user);
                $user->setToken($token);
                $this->em->flush($user);
            }
            $this->container->get('app.tools')->sendMailRegister($data,$token,'Mot de passe oublié','Reset');
            $resultat = $this->tools->getStateSession(200, 'Un mail a été envoyé vers votre adresse mail.');

        }else{
            $token = null;
            $resultat = $this->tools->getStateSession(200, 'Le compte n\'existe pas encors. Veuillez s\'inscrire');
        }

        return ['token'=> $token, 'resultat'=>$resultat];

    }

    public function testExistance($propriete,$value){

        $user = $this->em->getRepository('App:User')->findOneBy([$propriete => $value]);

        if($user){
            return true;
        }else{
            return false;
        }
    }

    private function upDateUser($data){
        $type = $this->em->getRepository('App:Type')->find(1);
        $user = $this->container->get('app.tools')->updateUser('add',$data,$type,true,"ROLE_ADMIN",0);
        return $user;

    }



}