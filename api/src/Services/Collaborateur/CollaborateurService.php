<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Collaborateur;


use App\Entity\Bureau;
use App\Entity\Cabinet;
use App\Entity\Collaborateur;
use App\Entity\DetExpert;
use App\Entity\Entites;
use App\Entity\Entreprise;
use App\Entity\ListeCollab;
use App\Services\ContainerService;
use App\Services\ToolsService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\Date;

class CollaborateurService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;

    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Collaborateur';
    }

    public function updateCollab($data, $mode){

        if($this->state['code']  == 200){

            $state = $this->existCollab($data,$mode);
            if($state['code'] == 401){
                return $state;
            }else{
                $type = $this->tools->getOneEntity('Type',2);
                $user = $this->tools->updateUser($mode,$data,$type,true,"ROLE_USER",$this->user->getId());

                if($mode == 'add'){
                    $collab = new Collaborateur();
                    $collab->setIdUser($user->getId());
                }else{
                    $collab = $this->tools->getOneEntityBy($this->entity,['idUser'=>$data['id']]);
                }

                $collab->setSkype($data['skype']);
                $collab->setImage($data['image']);

                $bureau = $this->tools->getOneEntity('Bureau',$data['idBureau']);
                $collab->setBureau($bureau);

                if($mode == 'add'){
                    $this->em->persist($collab);
                    $this->em->flush();

                    $message = 'Collaborateur ajouté';
                }else{
                    $this->em->flush();
                    $message = 'Collaborateur modifié';
                }

                return $this->tools->getStateSession(200,$message);
            }


        }else{
            return $this->state;
        }


    }


    private function existCollab($data,$mode){

        if($mode == 'add' ){
            $state = $this->tools->isExist('User',['email'=>$data['email']]);
            if($state['code'] == 401){
                return $this->tools->getStateSession(401,$data['email'].' Existant');
            }

            $state = $this->tools->isExist('User',['username'=>$data['prenom']]);
            if($state['code'] == 401){
                return $this->tools->getStateSession(401,$data['prenom'].' Existant');
            }

        }else{

            $user = $this->tools->getOneEntity('User',$data['id']);
            if($user->getPrenom() != $data['prenom']) {
                $state = $this->tools->isExist('User',['username'=>$data['prenom']]);
                if($state['code'] == 401){
                    return $this->tools->getStateSession(401,$data['prenom'].' Existant');
                }
            }
        }


        return $this->tools->getStateSession(200, 'ok');
    }


    public function deleteCollab($data){

        if($this->state['code']  == 200){
            $user = $this->tools->getOneEntity('User',$data['id']);
            $collab = $this->tools->getOneEntityBy($this->entity,['idUser'=>$user->getId()]);

            $libelle = $user->getEmail();
            $this->em->remove($user);
            $this->em->remove($collab);
            $this->em->flush();

            return $this->tools->getStateSession(200,$libelle.' supprimé');

        }else{
            return $this->state;
        }
    }


    public function getAllCollab($data){

        if($this->state['code']  == 200){

            $collaborateurs = $this->em->getRepository('App:User')->getAllByFilter($data,$this->user->getId());

            if(count($collaborateurs) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{

                return $collaborateurs;
            }

        }else{
            return $this->state;
        }

    }

    public function getOneCollabById($data){

        if($this->state['code'] == 200){

            $collab = $this->tools->getOneEntity('User',$data['id']);
            $collab1 = $this->tools->getOneEntityBy('Collaborateur',['idUser'=>$data['id']]);

            if($collab){
                $colaborateur = [
                    'id'=>$data['id'],
                    'nom'=>$collab->getNom(),
                    'prenom'=>$collab->getPrenom(),
                    'tel'=>$collab->getTel(),
                    'skype'=>$collab1->getSkype(),
                    'email'=>$collab->getEmail(),
                    'idFonction'=>$collab->getIdFonction(),
                    'idProfil'=>$collab->getIdProfil(),
                    'idBureau'=>$collab->getIdBureau(),
                    'image'=>$collab1->getImage()
                ];
                return $colaborateur;
            }else{
                return $this->tools->getStateSession(401, 'Pas de collaborateur');
            }
        }else{
            return $this->state;
        }

    }

    public function getAllCollabByName($data){
        if($this->state['code']  == 200){
            $type = $this->tools->getOneEntity('Type',2);
            $profils = $this->tools->getAllEntities('User',['nom'=>$data['nom'],'type'=>$type->getLibelle(),'idExpert'=> $this->user->getId()]);
            if(count($profils) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $profils;
            }
        }else{
            return $this->state;
        }

    }


    private function updateBureau($data,$user){
        foreach($data['bureau'] as $desk){
            $bureau = $this->em->getRepository('App:Bureau')->findOneBy(['user'=>$user,'nom'=>$desk['nom']]);
            if($bureau){
                $listeBureau[] = $bureau;
            }else{
                $bureau = new Bureau();
                $bureau->setUser($user);
                $bureau->setNom($desk['nom']);

                $cab = $this->em->getRepository('App:Cabinet')
                    ->findOneBy(['user'=>$user,'nomCommercial'=>$desk['cabinet']['nomCommercial']]);
                if($cab){
                    $cabinet = $cab;
                }else{
                    $cabinet = new Cabinet();
                    $cabinet->setUser($user);
                    $cabinet->setNomCommercial($desk['cabinet']['nomCommercial']);
                    $this->em->persist($cabinet);
                    $this->em->flush();
                }
                $bureau->setCabinet($cabinet);
                $listeBureau[] = $bureau;

                $this->em->persist($bureau);
                $this->em->flush();
            }

        }

        return $listeBureau;
    }

    private function updateEntreprise($data,$user){
        foreach($data['entreprise'] as $entrep){
            $entreprise = $this->em->getRepository('App:Entreprise')->findOneBy(['user'=>$user, 'nom'=>$entrep['nom']]);
            if($entreprise){
                $listeEntrep[] = $entreprise;
            }else{
                $entreprise = new Entreprise();
                $entreprise->setUser($user);
                $entreprise->setNom($entrep['nom']);
                $listeEntrep[] = $entreprise;

                $this->em->persist($entreprise);
                $this->em->flush();

            }
        }

        return $listeEntrep;
    }

    private function ajoutCollab($data){

        $type = $this->tools->getOneEntity('Type',2);
        $user = $this->tools->updateUser('add',$data,$type,true,"ROLE_USER",$this->user->getId());

        $collab = new Collaborateur();
        $collab->setIdUser($user->getId());
        $collab->setIdExpert($this->user->getId());
        $collab->setSkype($data['skype']);
        $collab->setValidite($data['validite']);
        $collab->setImage($data['image']);
        $collab->setIdFonction($data['idFonction']);
        $collab->setIdProfil($data['idProfil']);

        $bureau = $this->tools->getOneEntity('Bureau',$data['idBureau']);
        $collab->setBureau($bureau);

//        dump($collab);die;
       /* $listeBureau ;die;= $this->updateBureau($data,$userExpert);
        $listeEntrep = $this->updateEntreprise($data,$userExpert);
        $collab->setBureau($listeBureau);
        $collab->setEntreprise($listeEntrep);*/
        $this->em->persist($collab);
        $this->em->flush();

    }


}