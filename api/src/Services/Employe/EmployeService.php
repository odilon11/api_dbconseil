<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Employe;


use App\Entity\Bureau;
use App\Entity\Cabinet;
use App\Entity\Collaborateur;
use App\Entity\DetExpert;
use App\Entity\Employe;
use App\Entity\Entites;
use App\Entity\Entreprise;
use App\Services\ContainerService;
use App\Services\ToolsService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\Date;

class EmployeService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;

    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Employe';
    }


    public function addEmploye($data){

        if($this->state['code']  == 200){
            $state = $this->existEmploye($data);

            if($state['code'] == 401){
                return $state;
            }else{
                $this->ajoutEmploye($data);
                $type = $this->tools->getOneEntity('Type',3);
                return $this->tools->getStateSession(200,$type->getLibelle().' '.$data['nom'].' ajouté');
            }

        }else{
            return $this->state;
        }

    }

    private function ajoutEmploye($data){

        $type = $this->tools->getOneEntity('Type',3);
        $user = $this->tools->updateUser('add',$data,$type,true,"ROLE_USER",$this->user->getId());

        $employe = new Employe();
        $employe->setUser($user);
        $employe->setIdFonction($data['idFonction']);
        $employe->setSkype($data['skype']);
        $employe->setImage($data['image']);

        if(count($data['entreprise']) != 0){
            foreach($data['entreprise'] as $id){
                $employe->addEntreprise($this->tools->getOneEntity('Entreprise',$id));
            }
        }

        $this->em->persist($employe);
        $this->em->flush();

    }

    private function existEmploye($data){

        $state = $this->tools->isExist('User',['email'=>$data['email']]);

        if($state['code'] == 401){
            return $this->tools->getStateSession(401,$data['email'].' Existant');
        }

        $state = $this->tools->isExist('User',['username'=>$data['prenom']]);
        if($state['code'] == 401){
            return $this->tools->getStateSession(401,$data['prenom'].' Existant');
        }

        return $this->tools->getStateSession(200, 'ok');
    }


    public function editEmploye($data){

        if($this->state['code']  == 200){
            $user = $this->tools->getOneEntity('User',$data['id']);

            $state['code'] = 200;
            if(isset($data['prenom'])){
                $state = $this->tools->isExist('User',['username'=>$data['prenom']]);
                $user->setPrenom($data['prenom']);
                $user->setUsername($data['prenom']);
            }

            if($state['code'] == 401){
                return $this->tools->getStateSession(401,$data['prenom'].' Existant');
            }else{
                $user->setNom($data['nom']);
                $employe = $this->tools->getOneEntityBy($this->entity,['user'=> $user]);
                $employe->setIdFonction($data['idFonction']);
                $employe->setSkype($data['skype']);
                $employe->setImage($data['image']);

                foreach($employe->getEntreprise() as $entreprise){
                    $employe->removeEntreprise($entreprise);
                    $this->em->persist($employe);
                    $this->em->flush();
                }

                foreach($data['entreprise'] as $id){
                    $entreprise = $this->tools->getOneEntity('entreprise',$id);
                    $employe->addEntreprise($entreprise);
                    $this->em->flush();
                }

                return $this->tools->getStateSession(200,'Employé modifié');
            }

        }else{
            return $this->state;
        }

    }

    public function deleteEmploye($data){

        if($this->state['code']  == 200){
            $user = $this->tools->getOneEntity('User',$data['id']);
            $employe = $this->tools->getOneEntityBy($this->entity,['user'=>$user]);

            $libelle = $user->getEmail();
            $this->em->remove($user);
            $this->em->remove($employe);
            $this->em->flush();

            return $this->tools->getStateSession(200,'Employé '.$libelle.' supprimé');

        }else{
            return $this->state;
        }
    }

    public function getAllEmploye(){

        if($this->state['code']  == 200){
            $type = $this->tools->getOneEntity('Type',3);

            $employes = $this->tools->getAllEntities('User',['type'=>$type->getLibelle(),'idExpert'=> $this->user->getId()]);
            if(count($employes) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $employes;
            }

        }else{
            return $this->state;
        }

    }

    public function getOneEmployeById($data){

        if($this->state['code'] == 200){

            $employe = $this->tools->getOneEntity('User',$data['id']);
            if($employe){
                return $employe;
            }else{
                return $this->tools->getStateSession(401, 'Pas d\'employé');
            }
        }else{
            return $this->state;
        }
    }


    public function getAllEmployeByName($data){
        if($this->state['code']  == 200){
            $type = $this->tools->getOneEntity('Type',3);
            $profils = $this->tools->getAllEntities('User',['nom'=>$data['nom'],'type'=>$type->getLibelle(),'idExpert'=> $this->user->getId()]);
            if(count($profils) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $profils;
            }
        }else{
            return $this->state;
        }

    }

}