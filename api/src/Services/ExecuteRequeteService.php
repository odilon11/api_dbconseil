<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 6/25/18
 * Time: 10:17 PM
 */

namespace App\Services;


use Doctrine\ORM\EntityManager;

class ExecuteRequeteService
{

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function executeRequete($sql)
    {
        $connection = $this->em->getConnection();
        $statement = $connection->prepare($sql);
        $resultat = $statement->execute();
        return $resultat;
    }

}