<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services\Entites;


use App\Entity\DetExpert;
use App\Entity\Entites;
use App\Services\ContainerService;
use Symfony\Component\Validator\Constraints\Date;
use App\Services\ToolsService;
use Doctrine\ORM\EntityManager;

class EntitesService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;


    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Entites';
    }

    public function addEntites($data){


        if($this->state['code']  == 200){
            $state = $this->tools->isExist($this->entity,['user'=>$this->user,'libelle'=>$data['libelle']]);

            if($state['code'] == 401){
                return $state;
            }else{
                $type = $this->tools->getOneEntity('Type',$data['type']);
                $listeEntites = $this->tools->getAllEntities($this->entity,['user'=>$this->user,'type'=>$type->getLibelle()]);
                $entites = new Entites();

                if(count($listeEntites) == 0){
                    $entites->setDefaults(true);
                }else{
                    $entites->setDefaults(false);
                }
                $entites->setUser($this->user);
                $entites->setLibelle($data['libelle']);
                $entites->setDescription($data['description']);
                $entites->setInformation($data['information']);
                $entites->setType($type->getLibelle());

                $this->em->persist($entites);
                $this->em->flush();

                return $this->tools->getStateSession(200,$type->getLibelle().' '.$data['libelle'].' ajouté');
            }

        }else{
            return $this->state;
        }

    }

    public function editEntites($data){

        if($this->state['code']  == 200){

            $entites = $this->tools->getOneEntity($this->entity,$data['id']);
            $entites->setLibelle($data['libelle']);
            $entites->setDescription($data['description']);
            $entites->setInformation($data['information']);
            $this->em->flush();
            /*if(isset($data['autre'])){
                $entites->setAutre($data['autre']);
            }*/
            return $this->tools->getStateSession(200,$entites->getType().' '.$entites->getLibelle().' modifié');

        }else{
            return $this->state;
        }

    }

    public function deleteEntites($data){

        if($this->state['code']  == 200){
            $entites = $this->tools->getOneEntity($this->entity,$data['id']);
            $libelle = $entites->getLibelle();
            $this->em->remove($entites);
            $this->em->flush();

            return $this->tools->getStateSession(200,$entites->getType().' '.$libelle.' supprimé');

        }else{
            return $this->state;
        }

    }


    public function getAllEntites($data){

        if($this->state['code']  == 200){

            $type = $this->tools->getOneEntity('Type',$data['type']);

            $entites = $this->tools->getAllEntities($this->entity,['user'=>$this->user,'type'=>$type->getLibelle()]);

            if(count($entites) == 0){
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }else{
                return $entites;
            }

        }else{
            return $this->state;
        }

    }

    public function getOneEntiteById($data){

        if($this->state['code']  == 200){

            $entites = $this->tools->getOneEntity($this->entity,$data['id']);
            if($entites){
                return $entites;
            }else{
                return $this->tools->getStateSession(401,'Pas d\'enregistrement');
            }

        }else{
            return $this->state;
        }
    }

}