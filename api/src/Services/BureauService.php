<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/9/18
 * Time: 10:56 AM
 */
namespace App\Services;


use App\Entity\Bureau;
use App\Entity\Cabinet;
use App\Entity\Collaborateur;
use App\Entity\DetExpert;
use App\Entity\Entites;
use App\Entity\Entreprise;
use App\ToolsService;
use Symfony\Component\Validator\Constraints\Date;
use Doctrine\ORM\EntityManager;

class BureauService
{

    public $tools;
    public $em;
    public $state;
    public $user;
    public $entity;


    public function __construct(EntityManager $em, ToolsService $tools )
    {
        $this->tools = $tools;
        $this->state = $tools->getStatusByToken();
        $this->user = $tools->getUser();
        $this->em = $em;
        $this->entity = 'Bureau';
    }

    public function addBureau($data){

        if($this->state['code']  == 200){
            $status = $this->tools->isExist($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']]);
            if($status['code'] == 401){
                return $status;
            }else{
                return $this->updateBureau($data,'add');
            }
        }else{
            return $this->state;
        }
    }

    public function editBureau($data){
        if($this->state['code'] == 200){
           return $this->updateBureau($data,'edit');
        }else{
            return $this->state;
        }
    }

    public function updateBureau($data, $mode){


        if($mode == 'add'){
            $bureau = new Bureau();
        }else{
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
        }

            $bureau->setPersoMorale($data['persoMorale']);
            $bureau->setSiret1($data['siret1']);
            $bureau->setSiret2($data['siret2']);
            $bureau->setNomCommercial($data['nomCommercial']);
            $bureau->setTitre($data['titre']);
            $bureau->setNom($data['nom']);
            $bureau->setPrenom($data['prenom']);
            $bureau->setAdresse1($data['adresse1']);
            $bureau->setAdresse2($data['adresse2']);
            $bureau->setCp($data['cp']);
            $bureau->setVille($data['ville']);
            $bureau->setPays($data['pays']);
            $bureau->setEmail($data['email']);
            $bureau->setTelephone($data['tel']);
            $bureau->setSite($data['site']);
            $bureau->setImage($data['image']);
            $bureau->setIdInfoBanque($data['idInfoBanque']);

            if($mode == 'add'){
                $bureau->setUser($this->user);
                $this->em->persist($bureau);
                $this->em->flush();
                return $this->tools->getStateSession(200, 'Bureau ajouté');
            }else{
                $this->em->flush();
                return $this->tools->getStateSession(200, 'Bureau modifié');
            }


    }

    public function deleteBureau($data){

        if($this->state['code'] == 200){
            $bureau = $this->tools->getOneEntity($this->entity,$data['id']);
            if($bureau){
                $libelle = $bureau->getNomCommercial();
                $this->em->remove($bureau);
                $this->em->flush();
                return $this->tools->getStateSession(200, $libelle.' supprimé');
            }else{
                return $this->tools->getStateSession(401, 'Element non trouvé');
            }

        }else{
            return $this->state;
        }

    }


    public function getAllBureau(){

        if($this->state['code'] == 200){
            $bureaux = $this->tools->getAllEntities($this->entity,['user'=>$this->user]);
            if(count($bureaux) == 0){
                return $this->tools->getStateSession(401, 'Pas de bureau');
            }else{
                return $bureaux;
            }

        }else{
            return $this->state;
        }
    }

    public function getDataBureau($data){

        if($this->state['code'] == 200){
            $bureaux = $this->tools->getEntities($this->entity,['user'=>$this->user,'nomCommercial'=>$data['nomCommercial']]);
            if($bureaux){
                return $bureaux;
            }else{
                return $this->tools->getStateSession(401, 'Pas de bureau');
            }
        }else{
            return $this->state;
        }
    }

}
