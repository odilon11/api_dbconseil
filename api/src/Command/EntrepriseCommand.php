<?php
/**
 * Created by PhpStorm.
 * User: JOCELYN
 * Date: 15/09/2017
 * Time: 09:33
 */

namespace App\Command;



use App\Entity\ListeBureau;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EntrepriseCommand extends ContainerAwareCommand
{

    private $tools;
    private $em;

    protected function configure()
    {
        $this

            ->setName('app:entreprise')
            ->setDescription('Add liste bureau')
           /* ->addArgument('prenoms', InputArgument::REQUIRED, 'Votre prenom.')*/
            ->setHelp('Add liste bureau')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /*$output->writeln('Votre nom est : '.$input->getArgument('nom'));
        $output->writeln('Votre prenoms est : '.$input->getArgument('prenom'));*/

        $container = $this->getContainer();
        $this->em = $container->get('app.container')->em;
        $entreprises = $this->em->getRepository('App:Entreprise')->findAll();
        $actif = false;
        $actifVal = false;
        foreach($entreprises as $entreprise){
            $entreprise->setResponsable($entreprise->getNom().' '.$entreprise->getPrenom());
            if($actif){
                $actifVal = true;
                $actif = false;
            }else{
                $actifVal = false;
                $actif = true;
            }
            $entreprise->setActif($actifVal);
            $this->em->flush();
        }

        $output->writeln('Nombre d\'entreprise : '.count($entreprises));

    }



}