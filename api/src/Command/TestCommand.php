<?php
/**
 * Created by PhpStorm.
 * User: JOCELYN
 * Date: 15/09/2017
 * Time: 09:33
 */

namespace App\Command;



use App\Entity\ListeBureau;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{

    private $tools;
    private $em;

    protected function configure()
    {
        $this

            ->setName('app:test')
            ->setDescription('test')
           /* ->addArgument('prenoms', InputArgument::REQUIRED, 'Votre prenom.')*/
            ->setHelp('test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /*$output->writeln('Votre nom est : '.$input->getArgument('nom'));
        $output->writeln('Votre prenoms est : '.$input->getArgument('prenom'));*/
        $container = $this->getContainer();

        $this->generatePdf($output,$container);

//        $this->generateXls($output,$container);

    }

    private function generatePdf($output,$container){

        $pdf = $container->get('knp_snappy.pdf');
        $pdf->setTimeout(10000);
        $view = "test/test.html.twig";
        $parameters = ['nom'=>'Jocelyn'];
        $template = $container->get('templating')->render($view, $parameters, true);
        $reportFileName = __DIR__.'/../../../web/generate/pdf/test.pdf';
        if(file_exists($reportFileName)){
            unlink($reportFileName);
        }

        $pdf->generateFromHtml($template, $reportFileName);

        $output->writeln('Pdf créé : ');
    }

    private function generateXls($output,$container){

        $container->get('app.test')->generateXls();

        $output->writeln('Excel créé : ');
    }

}