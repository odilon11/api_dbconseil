<?php
/**
 * Created by PhpStorm.
 * User: JOCELYN
 * Date: 15/09/2017
 * Time: 09:33
 */

namespace App\Command;



use App\Entity\ListeBureau;
use App\Entity\ListeCollab;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CollaborateurCommand extends ContainerAwareCommand
{

    private $tools;
    private $em;

    protected function configure()
    {
        $this

            ->setName('app:collab')
            ->setDescription('Collaborateur')
           /* ->addArgument('prenoms', InputArgument::REQUIRED, 'Votre prenom.')*/
            ->setHelp('Collaborateur')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      //  $collabs = $this->setIdExpertCollab();
//        $collabs = $this->setListeCollab();
        $collabs = $this->setIdUserCollab();
        $nbuser = $this->setUtilisateur();
        $output->writeln('Nombre de collaborateur : '.count($collabs));

    }

    private function getCollab(){
        $container = $this->getContainer();
        $this->em = $container->get('app.container')->em;
        $collaborateurs = $this->em->getRepository('App:Collaborateur')->findAll();

        return $collaborateurs;
    }

    public function setUtilisateur(){
        $container = $this->getContainer();
        $this->em = $container->get('app.container')->em;
        $users = $this->em->getRepository('App:User')->findAll();

        foreach($users as $user){
            $utilisateur = $user->getNom().' '.$user->getPrenom();
            $user->setUtilisateur($utilisateur);
            $this->em->flush();
        }

        return $users;
    }

    private function setIdUserCollab(){
        $liste = [9,10,11,12,13];
        $collaborateurs = $this->getCollab();
        $k = 0;
        foreach($collaborateurs as $collaborateur){

            if($k > 4){
                $k = 0;
            }

            $user = $this->em->getRepository('App:User')->find($collaborateur->getIdUser());
            $user->setIdProfil($liste[$k]); //$collaborateur->getIdExpert()
            $user->setIdFonction($liste[$k]); //$collaborateur->getIdFonction()
            $idBureau = $collaborateur->getBureau() ? $collaborateur->getBureau()->getId() : 13;
            $user->setIdBureau($idBureau);

            $this->em->flush();
            $k++;
        }

        return count($collaborateurs);
    }

    private function setListeCollab(){

        $collaborateurs = $this->getCollab();
        $container = $this->getContainer();
        $container->get('app.execute-request')->executeRequete('TRUNCATE TABLE liste_collab');

        foreach($collaborateurs as $collaborateur){



            $listeCollab = new ListeCollab();
            $user = $this->em->getRepository('App:User')->find($collaborateur->getIdExpert());
            $collab = $this->em->getRepository('App:User')->find($collaborateur->getIdUser());

            $listeCollab->setUser($user);
            $listeCollab->setCollaborateur($collaborateur);


            $fonction = $this->em->getRepository('App:Profil')->find($collaborateur->getIdFonction());
            $profil = $this->em->getRepository('App:Profil')->find($collaborateur->getIdExpert());

            $utilisateur = $collab->getNom().' '.$collab->getPrenom();
            $listeCollab->setUtilisateur($utilisateur);
            $listeCollab->setEmail($collab->getEmail());
            $listeCollab->setValidite($collaborateur->getValidite());
            $listeCollab->setFonction($fonction->getLibelle());
            $listeCollab->setProfils($profil->getLibelle());

            $this->em->persist($listeCollab);
            $this->em->flush();
        }

            return count($collaborateurs);
    }

    private function setIdExpertCollab(){

        $container = $this->getContainer();
        $this->em = $container->get('app.container')->em;
        $collabs = $this->em->getRepository('App:Collaborateur')->findAll();

        foreach($collabs as $collab){
            $user = $this->em->getRepository('App:User')->find($collab->getIdUser());
            $collab->setIdExpert($user->getIdExpert());
            $this->em->flush();
        }

        return count($collabs);
    }

}