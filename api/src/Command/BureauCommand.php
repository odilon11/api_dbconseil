<?php
/**
 * Created by PhpStorm.
 * User: JOCELYN
 * Date: 15/09/2017
 * Time: 09:33
 */

namespace App\Command;



use App\Entity\ListeBureau;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BureauCommand extends ContainerAwareCommand
{

    private $tools;
    private $em;

    protected function configure()
    {
        $this

            ->setName('app:liste_bureau')
            ->setDescription('Add liste bureau')
           /* ->addArgument('prenoms', InputArgument::REQUIRED, 'Votre prenom.')*/
            ->setHelp('Add liste bureau')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /*$output->writeln('Votre nom est : '.$input->getArgument('nom'));
        $output->writeln('Votre prenoms est : '.$input->getArgument('prenom'));*/

        $container = $this->getContainer();
        $this->em = $container->get('app.container')->em;
        $bureaux = $this->em->getRepository('App:Bureau')->findAll();
        $container->get('app.execute-request')->executeRequete('TRUNCATE TABLE liste_bureau');
        $this->tools = $container->get('app.tools');
        $user = $container->get('fos_user.user_manager')->findUserByEmail('expert3@gmail.com');
        //$mode,$nom,$nomCom,$cpVille,$paiement,$nbColl,$nbClient,$user
        foreach($bureaux as $bureau){
                $paiement = $this->tools->getOneEntity('Entites',$bureau->getIdInfoBanque());

                $this->updateListeBureau('add',$bureau->getNom(),$bureau->getNomCommercial(),
                    $bureau->getCp().'-'.$bureau->getVille(),
                    $paiement->getLibelle(),count($bureau->getCollaborateur()),count($bureau->getEntreprise()),
                    $user,$bureau);

        }

        $output->writeln('Nombre de bureau : '.count($bureaux));

    }

    private function updateListeBureau($mode,$nom,$nomCom,$cpVille,$paiement,$nbColl,$nbClient,$user,$bureau){

        //  dump($user);die;
        if($mode == 'add'){
            $listeBureau = new ListeBureau();
            $listeBureau->setNomCommercial($nomCom);
            $listeBureau->setUser($user);
        }else{
            $listeBureau = $this->tools->getOneEntityBy('ListeBureau',['nomCommercial'=>$nomCom]);
        }

        $listeBureau->setNom($nom);
        $listeBureau->setCpVille($cpVille);
        $listeBureau->setPaiement($paiement);
        $listeBureau->setNbCollab($nbColl);
        $listeBureau->setNbClient($nbClient);
        $listeBureau->setBureau($bureau);

        if($mode == 'add'){
            $this->em->persist($listeBureau);
            $this->em->flush();
        }else{
            $this->em->persist($listeBureau);
        }

    }

}