<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entreprise
 *
 * @ORM\Table(name="entreprise")
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=50, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="siret1", type="string", length=50, nullable=true)
     */
    private $siret1;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_commercial", type="string", length=50, nullable=true)
     */
    private $nomCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="responsable", type="string", length=50, nullable=true)
     */
    private $responsable;


    /**
     * @var string
     *
     * @ORM\Column(name="adresse1", type="string", length=50, nullable=true)
     */
    private $adresse1;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse2", type="string", length=50, nullable=true)
     */
    private $adresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=50, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=50, nullable=true)
     */
    private $tel;


    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=50, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="activite1", type="string", length=50, nullable=true)
     */
    private $activite1;

    /**
     * @var string
     *
     * @ORM\Column(name="activite2", type="string", length=50, nullable=true)
     */
    private $activite2;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="string", length=50, nullable=true)
     */
    private $capital;

    /**
     * @var string
     *
     * @ORM\Column(name="social", type="string", length=50, nullable=true)
     */
    private $social;

    /**
     * @var int
     *
     * @ORM\Column(name="id_groupe", type="integer", nullable=true)
     */
    private $idGroupe;

    /**
     * @var int
     *
     * @ORM\Column(name="id_imposition", type="integer", nullable=true)
     */
    private $idImposition;

    /**
     * @var int
     *
     * @ORM\Column(name="id_regime_tva", type="integer", nullable=true)
     */
    private $idRegimeTva;

    /**
     * @var int
     *
     * @ORM\Column(name="id_regime_fiscal", type="integer", nullable=true)
     */
    private $idRegimeFiscal;

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_tva", type="integer", nullable=true)
     */
    private $idTypeTva;

    /**
     * @var string
     *
     * @ORM\Column(name="num_tva_infra1", type="string", length=50, nullable=true)
     */
    private $NumTvaInfra1;

    /**
     * @var string
     *
     * @ORM\Column(name="num_tva_infra2", type="string", length=50, nullable=true)
     */
    private $NumTvaInfra2;

    /**
     * @var string
     *
     * @ORM\Column(name="num_tva_infra3", type="string", length=50, nullable=true)
     */
    private $NumTvaInfra3;

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_compta", type="integer", nullable=true)
     */
    private $idTypeCompta;

    /**
     * @var int
     *
     * @ORM\Column(name="id_info_banque", type="integer", nullable=true)
     */
    private $idInfoBanque;

    /**
     * @var string
     *
     * @ORM\Column(name="greffe", type="string", length=50, nullable=true)
     */
    private $greffe;


    /**
     * @var string
     *
     * @ORM\Column(name="cloture", type="string", length=50, nullable=true)
     */
    private $cloture;



    /**
     * @var boolean
     *
     * @ORM\Column(name="activer_tous", type="boolean", nullable=true)
     */
    private $activerTous;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="ventes", type="boolean", nullable=true)
     */
    private $ventes;

    /**
     * @var bool
     *
     * @ORM\Column(name="achats", type="boolean", nullable=true)
     */
    private $achats;

    /**
     * @var bool
     *
     * @ORM\Column(name="ndf", type="boolean", nullable=true)
     */
    private $ndf;

    /**
     * @var bool
     *
     * @ORM\Column(name="caisse", type="boolean", nullable=true)
     */
    private $caisse;

    /**
     * @var bool
     *
     * @ORM\Column(name="banque", type="boolean", nullable=true)
     */
    private $banque;

    /**
     * @var bool
     *
     * @ORM\Column(name="tab_bord", type="boolean", nullable=true)
     */
    private $tabBord;

    /**
     * @var bool
     *
     * @ORM\Column(name="comptabilte", type="boolean", nullable=true)
     */
    private $comptabilte;

    /**
     * @var bool
     *
     * @ORM\Column(name="ged", type="boolean", nullable=true)
     */
    private $ged;


    /**
     * @var string
     *
     * @ORM\Column(name="affectation1", type="string", length=50, nullable=true)
     */
    private $affectation1;

    /**
     * @var string
     *
     * @ORM\Column(name="affectation2", type="string", length=50, nullable=true)
     */
    private $affectation2;

    /**
     * @var string
     *
     * @ORM\Column(name="affectation3", type="string", length=50, nullable=true)
     */
    private $affectation3;

    /**
     * @var string
     *
     * @ORM\Column(name="affectation4", type="string", length=50, nullable=true)
     */
    private $affectation4;

    /**
     * @var string
     *
     * @ORM\Column(name="affectation5", type="string", length=50, nullable=true)
     */
    private $affectation5;

    /**
     * @var string
     *
     * @ORM\Column(name="affectation_cab", type="string", length=50, nullable=true)
     */
    private $affectationCab;

    /**
     * @var bool
     *
     * @ORM\Column(name="perso_morale", type="boolean", nullable=true)
     */
    private $persoMorale;


    /**
     * @var string
     *
     * @ORM\Column(name="ventes_exp", type="string", nullable=true)
     */
    private $ventesExp;

    /**
     * @var string
     *
     * @ORM\Column(name="achats_exp", type="string", nullable=true)
     */
    private $achatsExp;

    /**
     * @var string
     *
     * @ORM\Column(name="ndf_exp", type="string", nullable=true)
     */
    private $ndfExp;

    /**
     * @var string
     *
     * @ORM\Column(name="caisse_exp", type="string", nullable=true)
     */
    private $caisseExp;

    /**
     * @var string
     *
     * @ORM\Column(name="banque_exp", type="string", nullable=true)
     */
    private $banqueExp;

    /**
     * @var string
     *
     * @ORM\Column(name="tab_bord_exp", type="string", nullable=true)
     */
    private $tabBordExp;

    /**
     * @var string
     *
     * @ORM\Column(name="comptabilte_exp", type="string", nullable=true)
     */
    private $comptabilteExp;

    /**
     * @var string
     *
     * @ORM\Column(name="ged_exp", type="string", nullable=true)
     */
    private $gedExp;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="entreprise")
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bureau", inversedBy="entreprise")
     */
    private $bureau;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ExoComptable")
     */
    private $exoComptable;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return Cabinet
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set nomCommercial
     *
     * @param string $nomCommercial
     *
     * @return Cabinet
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    /**
     * Get nomCommercial
     *
     * @return string
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Cabinet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Cabinet
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse1
     *
     * @param string $adresse1
     *
     * @return Cabinet
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    /**
     * Get adresse1
     *
     * @return string
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     *
     * @return Cabinet
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return Cabinet
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Cabinet
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Cabinet
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Cabinet
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Cabinet
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set activite
     *
     * @param string $activite
     *
     * @return Cabinet
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * Get activite
     *
     * @return string
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return Cabinet
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set greffe
     *
     * @param string $greffe
     *
     * @return Cabinet
     */
    public function setGreffe($greffe)
    {
        $this->greffe = $greffe;

        return $this;
    }

    /**
     * Get greffe
     *
     * @return string
     */
    public function getGreffe()
    {
        return $this->greffe;
    }


    /**
     * Set cloture
     *
     * @param string $cloture
     *
     * @return Cabinet
     */
    public function setCloture($cloture)
    {
        $this->cloture = $cloture;

        return $this;
    }

    /**
     * Get cloture
     *
     * @return string
     */
    public function getCloture()
    {
        return $this->cloture;
    }


    /**
     * Set ventes
     *
     * @param boolean $ventes
     *
     * @return Cabinet
     */
    public function setVentes($ventes)
    {
        $this->ventes = $ventes;

        return $this;
    }

    /**
     * Get ventes
     *
     * @return bool
     */
    public function getVentes()
    {
        return $this->ventes;
    }

    /**
     * Set achats
     *
     * @param boolean $achats
     *
     * @return Cabinet
     */
    public function setAchats($achats)
    {
        $this->achats = $achats;

        return $this;
    }

    /**
     * Get achats
     *
     * @return bool
     */
    public function getAchats()
    {
        return $this->achats;
    }

    /**
     * Set ndf
     *
     * @param boolean $ndf
     *
     * @return Cabinet
     */
    public function setNdf($ndf)
    {
        $this->ndf = $ndf;

        return $this;
    }

    /**
     * Get ndf
     *
     * @return bool
     */
    public function getNdf()
    {
        return $this->ndf;
    }

    /**
     * Set caisse
     *
     * @param boolean $caisse
     *
     * @return Cabinet
     */
    public function setCaisse($caisse)
    {
        $this->caisse = $caisse;

        return $this;
    }

    /**
     * Get caisse
     *
     * @return bool
     */
    public function getCaisse()
    {
        return $this->caisse;
    }

    /**
     * Set banque
     *
     * @param boolean $banque
     *
     * @return Cabinet
     */
    public function setBanque($banque)
    {
        $this->banque = $banque;

        return $this;
    }

    /**
     * Get banque
     *
     * @return bool
     */
    public function getBanque()
    {
        return $this->banque;
    }

    /**
     * Set tabBord
     *
     * @param boolean $tabBord
     *
     * @return Cabinet
     */
    public function setTabBord($tabBord)
    {
        $this->tabBord = $tabBord;

        return $this;
    }

    /**
     * Get tabBord
     *
     * @return bool
     */
    public function getTabBord()
    {
        return $this->tabBord;
    }

    /**
     * Set affectation1
     *
     * @param string $affectation1
     *
     * @return Cabinet
     */
    public function setAffectation1($affectation1)
    {
        $this->affectation1 = $affectation1;

        return $this;
    }

    /**
     * Get affectation1
     *
     * @return string
     */
    public function getAffectation1()
    {
        return $this->affectation1;
    }

    /**
     * Set affectation2
     *
     * @param string $affectation2
     *
     * @return Cabinet
     */
    public function setAffectation2($affectation2)
    {
        $this->affectation2 = $affectation2;

        return $this;
    }

    /**
     * Get affectation2
     *
     * @return string
     */
    public function getAffectation2()
    {
        return $this->affectation2;
    }

    /**
     * Set affectation3
     *
     * @param string $affectation3
     *
     * @return Cabinet
     */
    public function setAffectation3($affectation3)
    {
        $this->affectation3 = $affectation3;

        return $this;
    }

    /**
     * Get affectation3
     *
     * @return string
     */
    public function getAffectation3()
    {
        return $this->affectation3;
    }

    /**
     * Set affectation4
     *
     * @param string $affectation4
     *
     * @return Cabinet
     */
    public function setAffectation4($affectation4)
    {
        $this->affectation4 = $affectation4;

        return $this;
    }

    /**
     * Get affectation4
     *
     * @return string
     */
    public function getAffectation4()
    {
        return $this->affectation4;
    }

    /**
     * Set affectation5
     *
     * @param string $affectation5
     *
     * @return Cabinet
     */
    public function setAffectation5($affectation5)
    {
        $this->affectation5 = $affectation5;

        return $this;
    }

    /**
     * Get affectation5
     *
     * @return string
     */
    public function getAffectation5()
    {
        return $this->affectation5;
    }

    /**
     * Set affectationCab
     *
     * @param string $affectationCab
     *
     * @return Cabinet
     */
    public function setAffectationCab($affectationCab)
    {
        $this->affectationCab = $affectationCab;

        return $this;
    }

    /**
     * Get affectationCab
     *
     * @return string
     */
    public function getAffectationCab()
    {
        return $this->affectationCab;
    }

    /**
     * Set persoMorale
     *
     * @param boolean $persoMorale
     *
     * @return Cabinet
     */
    public function setPersoMorale($persoMorale)
    {
        $this->persoMorale = $persoMorale;

        return $this;
    }

    /**
     * Get persoMorale
     *
     * @return bool
     */
    public function getPersoMorale()
    {
        return $this->persoMorale;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Cabinet
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set bureau
     *
     * @param \App\Entity\Bureau $bureau
     *
     * @return Entreprise
     */
    public function setBureau(\App\Entity\Bureau $bureau = null)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return \App\Entity\Bureau
     */
    public function getBureau()
    {
        return $this->bureau;
    }

    /**
     * Set idGroupe
     *
     * @param integer $idGroupe
     *
     * @return Entreprise
     */
    public function setIdGroupe($idGroupe)
    {
        $this->idGroupe = $idGroupe;

        return $this;
    }

    /**
     * Get idGroupe
     *
     * @return integer
     */
    public function getIdGroupe()
    {
        return $this->idGroupe;
    }

    /**
     * Set idImposition
     *
     * @param integer $idImposition
     *
     * @return Entreprise
     */
    public function setIdImposition($idImposition)
    {
        $this->idImposition = $idImposition;

        return $this;
    }

    /**
     * Get idImposition
     *
     * @return integer
     */
    public function getIdImposition()
    {
        return $this->idImposition;
    }

    /**
     * Set idRegimeTva
     *
     * @param integer $idRegimeTva
     *
     * @return Entreprise
     */
    public function setIdRegimeTva($idRegimeTva)
    {
        $this->idRegimeTva = $idRegimeTva;

        return $this;
    }

    /**
     * Get idRegimeTva
     *
     * @return integer
     */
    public function getIdRegimeTva()
    {
        return $this->idRegimeTva;
    }

    /**
     * Set idRegimeFiscal
     *
     * @param integer $idRegimeFiscal
     *
     * @return Entreprise
     */
    public function setIdRegimeFiscal($idRegimeFiscal)
    {
        $this->idRegimeFiscal = $idRegimeFiscal;

        return $this;
    }

    /**
     * Get idRegimeFiscal
     *
     * @return integer
     */
    public function getIdRegimeFiscal()
    {
        return $this->idRegimeFiscal;
    }

    /**
     * Set idTypeTva
     *
     * @param integer $idTypeTva
     *
     * @return Entreprise
     */
    public function setIdTypeTva($idTypeTva)
    {
        $this->idTypeTva = $idTypeTva;

        return $this;
    }

    /**
     * Get idTypeTva
     *
     * @return integer
     */
    public function getIdTypeTva()
    {
        return $this->idTypeTva;
    }

    /**
     * Set numTvaInfra1
     *
     * @param string $numTvaInfra1
     *
     * @return Entreprise
     */
    public function setNumTvaInfra1($numTvaInfra1)
    {
        $this->NumTvaInfra1 = $numTvaInfra1;

        return $this;
    }

    /**
     * Get numTvaInfra1
     *
     * @return string
     */
    public function getNumTvaInfra1()
    {
        return $this->NumTvaInfra1;
    }

    /**
     * Set numTvaInfra2
     *
     * @param string $numTvaInfra2
     *
     * @return Entreprise
     */
    public function setNumTvaInfra2($numTvaInfra2)
    {
        $this->NumTvaInfra2 = $numTvaInfra2;

        return $this;
    }

    /**
     * Get numTvaInfra2
     *
     * @return string
     */
    public function getNumTvaInfra2()
    {
        return $this->NumTvaInfra2;
    }

    /**
     * Set numTvaInfra3
     *
     * @param string $numTvaInfra3
     *
     * @return Entreprise
     */
    public function setNumTvaInfra3($numTvaInfra3)
    {
        $this->NumTvaInfra3 = $numTvaInfra3;

        return $this;
    }

    /**
     * Get numTvaInfra3
     *
     * @return string
     */
    public function getNumTvaInfra3()
    {
        return $this->NumTvaInfra3;
    }

    /**
     * Set idTypeCompta
     *
     * @param integer $idTypeCompta
     *
     * @return Entreprise
     */
    public function setIdTypeCompta($idTypeCompta)
    {
        $this->idTypeCompta = $idTypeCompta;

        return $this;
    }

    /**
     * Get idTypeCompta
     *
     * @return integer
     */
    public function getIdTypeCompta()
    {
        return $this->idTypeCompta;
    }

    /**
     * Set idInfoBanque
     *
     * @param integer $idInfoBanque
     *
     * @return Entreprise
     */
    public function setIdInfoBanque($idInfoBanque)
    {
        $this->idInfoBanque = $idInfoBanque;

        return $this;
    }

    /**
     * Get idInfoBanque
     *
     * @return integer
     */
    public function getIdInfoBanque()
    {
        return $this->idInfoBanque;
    }

    /**
     * Set activite1
     *
     * @param string $activite1
     *
     * @return Entreprise
     */
    public function setActivite1($activite1)
    {
        $this->activite1 = $activite1;

        return $this;
    }

    /**
     * Get activite1
     *
     * @return string
     */
    public function getActivite1()
    {
        return $this->activite1;
    }

    /**
     * Set activite2
     *
     * @param string $activite2
     *
     * @return Entreprise
     */
    public function setActivite2($activite2)
    {
        $this->activite2 = $activite2;

        return $this;
    }

    /**
     * Get activite2
     *
     * @return string
     */
    public function getActivite2()
    {
        return $this->activite2;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Entreprise
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set activerTous
     *
     * @param boolean $activerTous
     *
     * @return Entreprise
     */
    public function setActiverTous($activerTous)
    {
        $this->activerTous = $activerTous;

        return $this;
    }

    /**
     * Get activerTous
     *
     * @return boolean
     */
    public function getActiverTous()
    {
        return $this->activerTous;
    }

    /**
     * Set comptabilte
     *
     * @param boolean $comptabilte
     *
     * @return Entreprise
     */
    public function setComptabilte($comptabilte)
    {
        $this->comptabilte = $comptabilte;

        return $this;
    }

    /**
     * Get comptabilte
     *
     * @return boolean
     */
    public function getComptabilte()
    {
        return $this->comptabilte;
    }

    /**
     * Set ged
     *
     * @param boolean $ged
     *
     * @return Entreprise
     */
    public function setGed($ged)
    {
        $this->ged = $ged;

        return $this;
    }

    /**
     * Get ged
     *
     * @return boolean
     */
    public function getGed()
    {
        return $this->ged;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Entreprise
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set siret1
     *
     * @param string $siret1
     *
     * @return Entreprise
     */
    public function setSiret1($siret1)
    {
        $this->siret1 = $siret1;

        return $this;
    }

    /**
     * Get siret1
     *
     * @return string
     */
    public function getSiret1()
    {
        return $this->siret1;
    }

    /**
     * Set social
     *
     * @param string $social
     *
     * @return Entreprise
     */
    public function setSocial($social)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get social
     *
     * @return string
     */
    public function getSocial()
    {
        return $this->social;
    }
    

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return Entreprise
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set ventesExp
     *
     * @param string $ventesExp
     *
     * @return Entreprise
     */
    public function setVentesExp($ventesExp)
    {
        $this->ventesExp = $ventesExp;

        return $this;
    }

    /**
     * Get ventesExp
     *
     * @return string
     */
    public function getVentesExp()
    {
        return $this->ventesExp;
    }

    /**
     * Set achatsExp
     *
     * @param string $achatsExp
     *
     * @return Entreprise
     */
    public function setAchatsExp($achatsExp)
    {
        $this->achatsExp = $achatsExp;

        return $this;
    }

    /**
     * Get achatsExp
     *
     * @return string
     */
    public function getAchatsExp()
    {
        return $this->achatsExp;
    }

    /**
     * Set ndfExp
     *
     * @param string $ndfExp
     *
     * @return Entreprise
     */
    public function setNdfExp($ndfExp)
    {
        $this->ndfExp = $ndfExp;

        return $this;
    }

    /**
     * Get ndfExp
     *
     * @return string
     */
    public function getNdfExp()
    {
        return $this->ndfExp;
    }

    /**
     * Set caisseExp
     *
     * @param string $caisseExp
     *
     * @return Entreprise
     */
    public function setCaisseExp($caisseExp)
    {
        $this->caisseExp = $caisseExp;

        return $this;
    }

    /**
     * Get caisseExp
     *
     * @return string
     */
    public function getCaisseExp()
    {
        return $this->caisseExp;
    }

    /**
     * Set banqueExp
     *
     * @param string $banqueExp
     *
     * @return Entreprise
     */
    public function setBanqueExp($banqueExp)
    {
        $this->banqueExp = $banqueExp;

        return $this;
    }

    /**
     * Get banqueExp
     *
     * @return string
     */
    public function getBanqueExp()
    {
        return $this->banqueExp;
    }

    /**
     * Set tabBordExp
     *
     * @param string $tabBordExp
     *
     * @return Entreprise
     */
    public function setTabBordExp($tabBordExp)
    {
        $this->tabBordExp = $tabBordExp;

        return $this;
    }

    /**
     * Get tabBordExp
     *
     * @return string
     */
    public function getTabBordExp()
    {
        return $this->tabBordExp;
    }

    /**
     * Set comptabilteExp
     *
     * @param string $comptabilteExp
     *
     * @return Entreprise
     */
    public function setComptabilteExp($comptabilteExp)
    {
        $this->comptabilteExp = $comptabilteExp;

        return $this;
    }

    /**
     * Get comptabilteExp
     *
     * @return string
     */
    public function getComptabilteExp()
    {
        return $this->comptabilteExp;
    }

    /**
     * Set gedExp
     *
     * @param string $gedExp
     *
     * @return Entreprise
     */
    public function setGedExp($gedExp)
    {
        $this->gedExp = $gedExp;

        return $this;
    }

    /**
     * Get gedExp
     *
     * @return string
     */
    public function getGedExp()
    {
        return $this->gedExp;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exoComptable = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add exoComptable
     *
     * @param \App\Entity\ExoComptable $exoComptable
     *
     * @return Entreprise
     */
    public function addExoComptable(\App\Entity\ExoComptable $exoComptable)
    {
        $this->exoComptable[] = $exoComptable;

        return $this;
    }

    /**
     * Remove exoComptable
     *
     * @param \App\Entity\ExoComptable $exoComptable
     */
    public function removeExoComptable(\App\Entity\ExoComptable $exoComptable)
    {
        $this->exoComptable->removeElement($exoComptable);
    }

    /**
     * Get exoComptable
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExoComptable()
    {
        return $this->exoComptable;
    }
}
