<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banque
 *
 * @ORM\Table(name="banque")
 * @ORM\Entity(repositoryClass="App\Repository\BanqueRepository")
 */
class Banque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50, nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="domiciliation", type="string", length=50, nullable=true)
     */
    private $domiciliation;

    /**
     * @var string
     *
     * @ORM\Column(name="ribIban", type="string", length=50, nullable=true)
     */
    private $ribIban;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defaults", type="boolean", nullable=true)
     */
    private $defaults;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Banque
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set domiciliation
     *
     * @param string $domiciliation
     *
     * @return Banque
     */
    public function setDomiciliation($domiciliation)
    {
        $this->domiciliation = $domiciliation;

        return $this;
    }

    /**
     * Get domiciliation
     *
     * @return string
     */
    public function getDomiciliation()
    {
        return $this->domiciliation;
    }

    /**
     * Set ribIban
     *
     * @param string $ribIban
     *
     * @return Banque
     */
    public function setRibIban($ribIban)
    {
        $this->ribIban = $ribIban;

        return $this;
    }

    /**
     * Get ribIban
     *
     * @return string
     */
    public function getRibIban()
    {
        return $this->ribIban;
    }

    /**
     * Set defaults
     *
     * @param boolean $defaults
     *
     * @return Banque
     */
    public function setDefaults($defaults)
    {
        $this->defaults = $defaults;

        return $this;
    }

    /**
     * Get defaults
     *
     * @return boolean
     */
    public function getDefaults()
    {
        return $this->defaults;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Banque
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
