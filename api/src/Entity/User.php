<?php
/**
 * Created by PhpStorm.
 * User: jocelyn
 * Date: 5/7/18
 * Time: 9:44 AM
 */

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true, unique=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50, nullable=true, unique=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="utilisateur", type="string", length=50, nullable=true, unique=false)
     */
    private $utilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=50, nullable=true, unique=false)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=true)
     */

    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=true)
     */

    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="text",nullable=true)
     */

    private $token;

    /**
     * @var int
     * @ORM\Column(name="id_expert",type="integer",nullable=true)
     */
    private $idExpert;

    /**
     * @var \string
     *
     * @ORM\Column(name="date_create", type="string", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \string
     *
     * @ORM\Column(name="validite", type="string", nullable=true)
     */
    private $validite;

    /**
     * @var \string
     *
     * @ORM\Column(name="date_active", type="string", nullable=true)
     */
    private $dateActive;

    /**
     * @var int
     * @ORM\Column(name="id_profil",type="integer",nullable=true)
     */
    private $idProfil;

    /**
     * @var int
     * @ORM\Column(name="id_fonction",type="integer",nullable=true)
     */
    private $idFonction;

    /**
     * @var int
     * @ORM\Column(name="id_bureau",type="integer",nullable=true)
     */
    private $idBureau;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Entites", mappedBy="user")
     */
    private $entites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cabinet", mappedBy="user")
     */
    private $cabinet;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Entreprise", mappedBy="user")
     */
    private $entreprise;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bureau", mappedBy="user")
     */
    private $bureau;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Profil", mappedBy="user")
     */
    private $profil;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ListeBureau", mappedBy="user")
     */
    private $listeBureau;

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    



    /**
     * Set dateCreate
     *
     * @param string $dateCreate
     *
     * @return User
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return string
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateActive
     *
     * @param string $dateActive
     *
     * @return User
     */
    public function setDateActive($dateActive)
    {
        $this->dateActive = $dateActive;

        return $this;
    }

    /**
     * Get dateActive
     *
     * @return string
     */
    public function getDateActive()
    {
        return $this->dateActive;
    }

   

    /**
     * Add entite
     *
     * @param \App\Entity\Entites $entite
     *
     * @return User
     */
    public function addEntite(\App\Entity\Entites $entite)
    {
        $this->entites[] = $entite;

        return $this;
    }

    /**
     * Remove entite
     *
     * @param \App\Entity\Entites $entite
     */
    public function removeEntite(\App\Entity\Entites $entite)
    {
        $this->entites->removeElement($entite);
    }

    /**
     * Get entites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntites()
    {
        return $this->entites;
    }

    /**
     * Add cabinet
     *
     * @param \App\Entity\Cabinet $cabinet
     *
     * @return User
     */
    public function addCabinet(\App\Entity\Cabinet $cabinet)
    {
        $this->cabinet[] = $cabinet;

        return $this;
    }

    /**
     * Remove cabinet
     *
     * @param \App\Entity\Cabinet $cabinet
     */
    public function removeCabinet(\App\Entity\Cabinet $cabinet)
    {
        $this->cabinet->removeElement($cabinet);
    }

    /**
     * Get cabinet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCabinet()
    {
        return $this->cabinet;
    }


    public function addEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise[] = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     */
    public function removeEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise->removeElement($entreprise);
    }

    /**
     * Get entreprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * Add bureau
     *
     * @param \App\Entity\Bureau $bureau
     *
     * @return User
     */
    public function addBureau(\App\Entity\Bureau $bureau)
    {
        $this->bureau[] = $bureau;

        return $this;
    }

    /**
     * Remove bureau
     *
     * @param \App\Entity\Bureau $bureau
     */
    public function removeBureau(\App\Entity\Bureau $bureau)
    {
        $this->bureau->removeElement($bureau);
    }

    /**
     * Get bureau
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBureau()
    {
        return $this->bureau;
    }

    /**
     * Set idExpert
     *
     * @param integer $idExpert
     *
     * @return User
     */
    public function setIdExpert($idExpert)
    {
        $this->idExpert = $idExpert;

        return $this;
    }

    /**
     * Get idExpert
     *
     * @return integer
     */
    public function getIdExpert()
    {
        return $this->idExpert;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Add profil
     *
     * @param \App\Entity\Profil $profil
     *
     * @return User
     */
    public function addProfil(\App\Entity\Profil $profil)
    {
        $this->profil[] = $profil;

        return $this;
    }

    /**
     * Remove profil
     *
     * @param \App\Entity\Profil $profil
     */
    public function removeProfil(\App\Entity\Profil $profil)
    {
        $this->profil->removeElement($profil);
    }

    /**
     * Get profil
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set validite
     *
     * @param string $validite
     *
     * @return User
     */
    public function setValidite($validite)
    {
        $this->validite = $validite;

        return $this;
    }

    /**
     * Get validite
     *
     * @return string
     */
    public function getValidite()
    {
        return $this->validite;
    }

    /**
     * Add listeBureau
     *
     * @param \App\Entity\ListeBureau $listeBureau
     *
     * @return User
     */
    public function addListeBureau(\App\Entity\ListeBureau $listeBureau)
    {
        $this->listeBureau[] = $listeBureau;

        return $this;
    }

    /**
     * Remove listeBureau
     *
     * @param \App\Entity\ListeBureau $listeBureau
     */
    public function removeListeBureau(\App\Entity\ListeBureau $listeBureau)
    {
        $this->listeBureau->removeElement($listeBureau);
    }

    /**
     * Get listeBureau
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListeBureau()
    {
        return $this->listeBureau;
    }

    /**
     * Set idProfil
     *
     * @param integer $idProfil
     *
     * @return User
     */
    public function setIdProfil($idProfil)
    {
        $this->idProfil = $idProfil;

        return $this;
    }

    /**
     * Get idProfil
     *
     * @return integer
     */
    public function getIdProfil()
    {
        return $this->idProfil;
    }

    /**
     * Set idFonction
     *
     * @param integer $idFonction
     *
     * @return User
     */
    public function setIdFonction($idFonction)
    {
        $this->idFonction = $idFonction;

        return $this;
    }

    /**
     * Get idFonction
     *
     * @return integer
     */
    public function getIdFonction()
    {
        return $this->idFonction;
    }

    /**
     * Set idBureau
     *
     * @param integer $idBureau
     *
     * @return User
     */
    public function setIdBureau($idBureau)
    {
        $this->idBureau = $idBureau;

        return $this;
    }

    /**
     * Get idBureau
     *
     * @return integer
     */
    public function getIdBureau()
    {
        return $this->idBureau;
    }

    /**
     * Set utilisateur
     *
     * @param string $utilisateur
     *
     * @return User
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return string
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
