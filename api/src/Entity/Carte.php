<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carte
 *
 * @ORM\Table(name="carte")
 * @ORM\Entity(repositoryClass="App\Repository\CarteRepository")
 */
class Carte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="num1", type="string", length=50, nullable=true)
     */
    private $num1;

    /**
     * @var string
     *
     * @ORM\Column(name="num2", type="string", length=50, nullable=true)
     */
    private $num2;

    /**
     * @var string
     *
     * @ORM\Column(name="num3", type="string", length=50, nullable=true)
     */
    private $num3;

    /**
     * @var string
     *
     * @ORM\Column(name="num4", type="string", length=50, nullable=true)
     */
    private $num4;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_carte", type="string", length=50, nullable=true)
     */
    private $numeroCarte;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=50, nullable=true)
     */
    private $mois;

    /**
     * @var string
     *
     * @ORM\Column(name="anne", type="string", length=50, nullable=true)
     */
    private $anne;

    /**
     * @var string
     *
     * @ORM\Column(name="ccv", type="string", length=50, nullable=true)
     */
    private $ccv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num1
     *
     * @param string $num1
     *
     * @return Carte
     */
    public function setNum1($num1)
    {
        $this->num1 = $num1;

        return $this;
    }

    /**
     * Get num1
     *
     * @return string
     */
    public function getNum1()
    {
        return $this->num1;
    }

    /**
     * Set num2
     *
     * @param string $num2
     *
     * @return Carte
     */
    public function setNum2($num2)
    {
        $this->num2 = $num2;

        return $this;
    }

    /**
     * Get num2
     *
     * @return string
     */
    public function getNum2()
    {
        return $this->num2;
    }

    /**
     * Set num3
     *
     * @param string $num3
     *
     * @return Carte
     */
    public function setNum3($num3)
    {
        $this->num3 = $num3;

        return $this;
    }

    /**
     * Get num3
     *
     * @return string
     */
    public function getNum3()
    {
        return $this->num3;
    }

    /**
     * Set num4
     *
     * @param string $num4
     *
     * @return Carte
     */
    public function setNum4($num4)
    {
        $this->num4 = $num4;

        return $this;
    }

    /**
     * Get num4
     *
     * @return string
     */
    public function getNum4()
    {
        return $this->num4;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return Carte
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set anne
     *
     * @param string $anne
     *
     * @return Carte
     */
    public function setAnne($anne)
    {
        $this->anne = $anne;

        return $this;
    }

    /**
     * Get anne
     *
     * @return string
     */
    public function getAnne()
    {
        return $this->anne;
    }

    /**
     * Set ccv
     *
     * @param string $ccv
     *
     * @return Carte
     */
    public function setCcv($ccv)
    {
        $this->ccv = $ccv;

        return $this;
    }

    /**
     * Get ccv
     *
     * @return string
     */
    public function getCcv()
    {
        return $this->ccv;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Carte
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set numeroCarte
     *
     * @param string $numeroCarte
     *
     * @return Carte
     */
    public function setNumeroCarte($numeroCarte)
    {
        $this->numeroCarte = $numeroCarte;

        return $this;
    }

    /**
     * Get numeroCarte
     *
     * @return string
     */
    public function getNumeroCarte()
    {
        return $this->numeroCarte;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Carte
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
