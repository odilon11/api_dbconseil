<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bureau
 *
 * @ORM\Table(name="bureau")
 * @ORM\Entity(repositoryClass="App\Repository\BureauRepository")
 */
class Bureau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="perso_morale", type="boolean", nullable=true)
     */
    private $persoMorale;
    
    /**
     * @var string
     *
     * @ORM\Column(name="siret1", type="string", length=50, nullable=true)
     */
    private $siret1;

    /**
     * @var string
     *
     * @ORM\Column(name="siret2", type="string", length=50, nullable=true)
     */
    private $siret2;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=10, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse1", type="string", length=50, nullable=true)
     */
    private $adresse1;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse2", type="string", length=50, nullable=true)
     */
    private $adresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_commercial", type="string", length=50, nullable=true)
     */
    private $nomCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=50, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=50, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=50, nullable=true)
     */
    private $telephone;

    /**
     * @var int
     *
     * @ORM\Column(name="id_info_banque", type="integer", nullable=true)
     */
    private $idInfoBanque;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bureau")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cabinet", inversedBy="bureau")
     */
    private $cabinet;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Entreprise", mappedBy="bureau")
     */
    private $entreprise;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Collaborateur", mappedBy="bureau")
     *
     */
    private $collaborateur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Bureau
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse1
     *
     * @param string $adresse1
     *
     * @return Bureau
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    /**
     * Get adresse1
     *
     * @return string
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     *
     * @return Bureau
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set nomCommercial
     *
     * @param string $nomCommercial
     *
     * @return Bureau
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    /**
     * Get nomCommercial
     *
     * @return string
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return Bureau
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Bureau
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Bureau
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Bureau
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Bureau
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Bureau
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Bureau
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cabinet
     *
     * @param \App\Entity\Cabinet $cabinet
     *
     * @return Bureau
     */
    public function setCabinet(\App\Entity\Cabinet $cabinet = null)
    {
        $this->cabinet = $cabinet;

        return $this;
    }

    /**
     * Get cabinet
     *
     * @return \App\Entity\Cabinet
     */
    public function getCabinet()
    {
        return $this->cabinet;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entreprise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     *
     * @return Bureau
     */
    public function addEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise[] = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     */
    public function removeEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise->removeElement($entreprise);
    }

    /**
     * Get entreprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Bureau
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Bureau
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set idInfoBanque
     *
     * @param integer $idInfoBanque
     *
     * @return Bureau
     */
    public function setIdInfoBanque($idInfoBanque)
    {
        $this->idInfoBanque = $idInfoBanque;

        return $this;
    }

    /**
     * Get idInfoBanque
     *
     * @return integer
     */
    public function getIdInfoBanque()
    {
        return $this->idInfoBanque;
    }

    /**
     * Set persoMorale
     *
     * @param boolean $persoMorale
     *
     * @return Bureau
     */
    public function setPersoMorale($persoMorale)
    {
        $this->persoMorale = $persoMorale;

        return $this;
    }

    /**
     * Get persoMorale
     *
     * @return boolean
     */
    public function getPersoMorale()
    {
        return $this->persoMorale;
    }

    /**
     * Set siret1
     *
     * @param string $siret1
     *
     * @return Bureau
     */
    public function setSiret1($siret1)
    {
        $this->siret1 = $siret1;

        return $this;
    }

    /**
     * Get siret1
     *
     * @return string
     */
    public function getSiret1()
    {
        return $this->siret1;
    }

    /**
     * Set siret2
     *
     * @param string $siret2
     *
     * @return Bureau
     */
    public function setSiret2($siret2)
    {
        $this->siret2 = $siret2;

        return $this;
    }

    /**
     * Get siret2
     *
     * @return string
     */
    public function getSiret2()
    {
        return $this->siret2;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Bureau
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Add collaborateur
     *
     * @param \App\Entity\Collaborateur $collaborateur
     *
     * @return Bureau
     */
    public function addCollaborateur(\App\Entity\Collaborateur $collaborateur)
    {
        $this->collaborateur[] = $collaborateur;

        return $this;
    }

    /**
     * Remove collaborateur
     *
     * @param \App\Entity\Collaborateur $collaborateur
     */
    public function removeCollaborateur(\App\Entity\Collaborateur $collaborateur)
    {
        $this->collaborateur->removeElement($collaborateur);
    }

    /**
     * Get collaborateur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaborateur()
    {
        return $this->collaborateur;
    }
}
