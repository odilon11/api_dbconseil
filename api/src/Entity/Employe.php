<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Employe
 *
 * @ORM\Table(name="employe")
 * @ORM\Entity(repositoryClass="App\Repository\EmployeRepository")
 */
class Employe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_fonction", type="integer", nullable=true)
     */
    private $idFonction;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=250)
     */
    private $image;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Entreprise")
     */
    private $entreprise;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Employe
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entreprise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     *
     * @return Employe
     */
    public function addEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise[] = $entreprise;

        return $this;
    }

    /**
     * Set entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     *
     * @return Employe
     */
    public function setEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     */
    public function removeEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise->removeElement($entreprise);
    }

    /**
     * Get entreprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }




    /**
     * Set idFonction
     *
     * @param integer $idFonction
     *
     * @return Employe
     */
    public function setIdFonction($idFonction)
    {
        $this->idFonction = $idFonction;

        return $this;
    }

    /**
     * Get idFonction
     *
     * @return integer
     */
    public function getIdFonction()
    {
        return $this->idFonction;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Employe
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Employe
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}
