<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListeBureau
 *
 * @ORM\Table(name="liste_bureau")
 * @ORM\Entity(repositoryClass="App\Repository\ListeBureauRepository")
 */
class ListeBureau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCommercial", type="string", length=255, unique=true)
     */
    private $nomCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="cpVille", type="string", length=50, nullable=true)
     */
    private $cpVille;

    /**
     * @var string
     *
     * @ORM\Column(name="paiement", type="string", length=50, nullable=true)
     */
    private $paiement;

    /**
     * @var int
     *
     * @ORM\Column(name="nbCollab", type="integer", nullable=true)
     */
    private $nbCollab;

    /**
     * @var int
     *
     * @ORM\Column(name="nbClient", type="integer", nullable=true)
     */
    private $nbClient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="listeBureau")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Bureau")
     */
    private $bureau;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return ListeBureau
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set nomCommercial
     *
     * @param string $nomCommercial
     *
     * @return ListeBureau
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    /**
     * Get nomCommercial
     *
     * @return string
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * Set cpVille
     *
     * @param string $cpVille
     *
     * @return ListeBureau
     */
    public function setCpVille($cpVille)
    {
        $this->cpVille = $cpVille;

        return $this;
    }

    /**
     * Get cpVille
     *
     * @return string
     */
    public function getCpVille()
    {
        return $this->cpVille;
    }

    /**
     * Set paiement
     *
     * @param string $paiement
     *
     * @return ListeBureau
     */
    public function setPaiement($paiement)
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * Get paiement
     *
     * @return string
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Set nbCollab
     *
     * @param integer $nbCollab
     *
     * @return ListeBureau
     */
    public function setNbCollab($nbCollab)
    {
        $this->nbCollab = $nbCollab;

        return $this;
    }

    /**
     * Get nbCollab
     *
     * @return int
     */
    public function getNbCollab()
    {
        return $this->nbCollab;
    }

    /**
     * Set nbClient
     *
     * @param integer $nbClient
     *
     * @return ListeBureau
     */
    public function setNbClient($nbClient)
    {
        $this->nbClient = $nbClient;

        return $this;
    }

    /**
     * Get nbClient
     *
     * @return int
     */
    public function getNbClient()
    {
        return $this->nbClient;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return ListeBureau
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set bureau
     *
     * @param \App\Entity\Bureau $bureau
     *
     * @return ListeBureau
     */
    public function setBureau(\App\Entity\Bureau $bureau = null)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return \App\Entity\Bureau
     */
    public function getBureau()
    {
        return $this->bureau;
    }
}
