<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paypal
 *
 * @ORM\Table(name="paypal")
 * @ORM\Entity(repositoryClass="App\Repository\PaypalRepository")
 */
class Paypal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="numero_carte", type="string", length=50, nullable=true)
     */
    private $numeroCarte;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=50, nullable=true)
     */
    private $mois;

    /**
     * @var string
     *
     * @ORM\Column(name="anne", type="string", length=50, nullable=true)
     */
    private $anne;

    /**
     * @var string
     *
     * @ORM\Column(name="crypto", type="string", length=50, nullable=true)
     */
    private $crypto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroCarte
     *
     * @param string $numeroCarte
     *
     * @return Paypal
     */
    public function setNumeroCarte($numeroCarte)
    {
        $this->numeroCarte = $numeroCarte;

        return $this;
    }

    /**
     * Get numeroCarte
     *
     * @return string
     */
    public function getNumeroCarte()
    {
        return $this->numeroCarte;
    }

    /**
     * Set crypto
     *
     * @param string $crypto
     *
     * @return Paypal
     */
    public function setCrypto($crypto)
    {
        $this->crypto = $crypto;

        return $this;
    }

    /**
     * Get crypto
     *
     * @return string
     */
    public function getCrypto()
    {
        return $this->crypto;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Paypal
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Paypal
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return Paypal
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set anne
     *
     * @param string $anne
     *
     * @return Paypal
     */
    public function setAnne($anne)
    {
        $this->anne = $anne;

        return $this;
    }

    /**
     * Get anne
     *
     * @return string
     */
    public function getAnne()
    {
        return $this->anne;
    }
}
