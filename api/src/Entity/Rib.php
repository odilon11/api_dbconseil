<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rib
 *
 * @ORM\Table(name="rib")
 * @ORM\Entity(repositoryClass="App\Repository\RibRepository")
 */
class Rib
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=50, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="domiciliation", type="string", length=50, nullable=true)
     */
    private $domiciliation;

    /**
     * @var string
     *
     * @ORM\Column(name="banque", type="string", length=50, nullable=true)
     */
    private $banque;

    /**
     * @var string
     *
     * @ORM\Column(name="guichet", type="string", length=50, nullable=true)
     */
    private $guichet;

    /**
     * @var string
     *
     * @ORM\Column(name="compte", type="string", length=50, nullable=true)
     */
    private $compte;

    /**
     * @var string
     *
     * @ORM\Column(name="cle", type="string", length=50, nullable=true)
     */
    private $cle;

    /**
     * @var string
     *
     * @ORM\Column(name="rib", type="string", length=50, nullable=true)
     */
    private $rib;

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=50, nullable=true)
     */
    private $iban;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=50, nullable=true)
     */
    private $bic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domiciliation
     *
     * @param string $domiciliation
     *
     * @return Rib
     */
    public function setDomiciliation($domiciliation)
    {
        $this->domiciliation = $domiciliation;

        return $this;
    }

    /**
     * Get domiciliation
     *
     * @return string
     */
    public function getDomiciliation()
    {
        return $this->domiciliation;
    }

    /**
     * Set banque
     *
     * @param string $banque
     *
     * @return Rib
     */
    public function setBanque($banque)
    {
        $this->banque = $banque;

        return $this;
    }

    /**
     * Get banque
     *
     * @return string
     */
    public function getBanque()
    {
        return $this->banque;
    }

    /**
     * Set guichet
     *
     * @param string $guichet
     *
     * @return Rib
     */
    public function setGuichet($guichet)
    {
        $this->guichet = $guichet;

        return $this;
    }

    /**
     * Get guichet
     *
     * @return string
     */
    public function getGuichet()
    {
        return $this->guichet;
    }

    /**
     * Set compte
     *
     * @param string $compte
     *
     * @return Rib
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;

        return $this;
    }

    /**
     * Get compte
     *
     * @return string
     */
    public function getCompte()
    {
        return $this->compte;
    }

    /**
     * Set cle
     *
     * @param string $cle
     *
     * @return Rib
     */
    public function setCle($cle)
    {
        $this->cle = $cle;

        return $this;
    }

    /**
     * Get cle
     *
     * @return string
     */
    public function getCle()
    {
        return $this->cle;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return Rib
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return Rib
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Rib
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Rib
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Rib
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set rib
     *
     * @param string $rib
     *
     * @return Rib
     */
    public function setRib($rib)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib
     *
     * @return string
     */
    public function getRib()
    {
        return $this->rib;
    }
}
