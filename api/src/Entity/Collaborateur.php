<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collaborateur
 *
 * @ORM\Table(name="collaborateur")
 * @ORM\Entity(repositoryClass="App\Repository\CollaborateurRepository")
 */
class Collaborateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=50, nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var int
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bureau", inversedBy="collaborateur")
     */
    private $bureau;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Entreprise")
     */
    private $entreprise;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Collaborateur
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }



    /**
     * Set image
     *
     * @param string $image
     *
     * @return Collaborateur
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bureau = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entreprise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bureau
     *
     * @param \App\Entity\Bureau $bureau
     *
     * @return Collaborateur
     */
    public function addBureau(\App\Entity\Bureau $bureau)
    {
        $this->bureau[] = $bureau;

        return $this;
    }

    /**
     * Remove bureau
     *
     * @param \App\Entity\Bureau $bureau
     */
    public function removeBureau(\App\Entity\Bureau $bureau)
    {
        $this->bureau->removeElement($bureau);
    }

    /**
     * set bureau
     *
     * @return Collaborateur
     */
    public function setBureau($bureau)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBureau()
    {
        return $this->bureau;
    }



    /**
     * Add entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     *
     * @return Collaborateur
     */
    public function addEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise[] = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \App\Entity\Entreprise $entreprise
     */
    public function removeEntreprise(\App\Entity\Entreprise $entreprise)
    {
        $this->entreprise->removeElement($entreprise);
    }

    /**
     * set bureau
     *
     * @return Entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }


    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Collaborateur
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

   
}
