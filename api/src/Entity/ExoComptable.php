<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExoCompable
 *
 * @ORM\Table(name="exo_comptable")
 * @ORM\Entity(repositoryClass="App\Repository\ExoComptableRepository")
 */
class ExoComptable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="exercice", type="integer", nullable=true)
     */
    private $exercice;

    /**
     * @var string
     *
     * @ORM\Column(name="debut", type="string", length=50, nullable=true)
     */
    private $debut;

    /**
     * @var int
     *
     * @ORM\Column(name="duree", type="integer", nullable=true)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="fin", type="string", length=50, nullable=true)
     */
    private $fin;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=10, nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercice
     *
     * @param integer $exercice
     *
     * @return ExoCompable
     */
    public function setExercice($exercice)
    {
        $this->exercice = $exercice;

        return $this;
    }

    /**
     * Get exercice
     *
     * @return int
     */
    public function getExercice()
    {
        return $this->exercice;
    }

    /**
     * Set debut
     *
     * @param string $debut
     *
     * @return ExoCompable
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;

        return $this;
    }

    /**
     * Get debut
     *
     * @return string
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return ExoCompable
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return int
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set fin
     *
     * @param string $fin
     *
     * @return ExoCompable
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return string
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return ExoCompable
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return ExoComptable
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
