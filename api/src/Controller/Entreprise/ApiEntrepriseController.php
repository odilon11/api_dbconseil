<?php

namespace App\Controller\Entreprise;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
class ApiEntrepriseController extends Controller
{
    /**
     * @Route(
     *     name="api_add_entreprise",
     *     path="/api/entreprise/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entreprise')->addEntreprise($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_entreprise",
     *     path="/api/entreprise/edit",
     *     methods={"POST"}
     * )
     */
    public function editAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entreprise')->editEntreprise($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_detele_entreprise",
     *     path="/api/entreprise/detele",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entreprise')->deleteEntreprise($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_entreprise",
     *     path="/api/entreprise/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entreprise')->getAllEntreprise($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_one_entreprise",
     *     path="/api/entreprise/one",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entreprise')->getOneEntrepriseById($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
