<?php

namespace App\Controller\Profil;

use App\Entity\Fonction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiFonctionController extends Controller
{
    /**
     * @Route(
     *     name="api_add_fonction",
     *     path="/api/fonction/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.fonction')->updateFonction($tab,'add');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_fonction",
     *     path="/api/fonction/edit",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.fonction')->updateFonction($tab,'edit');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_fonction",
     *     path="/api/fonction/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.fonction')->deleteFonction($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_fonction",
     *     path="/api/fonction/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
//        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.fonction')->getAllFonction();
        $data = [];
        foreach ($res as $profil){
            $data[] =$profil->getData();
        }
        $view = $this->get('app.tools')->sendDataJson($data);
        return $view;
    }

    /**
     * @Route(
     *     name="api_one_fonction",
     *     path="/api/fonction/one",
     *     methods={"POST"}
     * )
     */
    public function findAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.fonction')->getAllFonctionById($tab);
        if($res instanceof Fonction){
            $view = $this->get('app.tools')->sendDataJson($res->getData());
        }else{
            $view = $this->get('app.tools')->sendDataJson($res);
        }

        return $view;
    }


}
