<?php

namespace App\Controller\Profil;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiProfilController extends Controller
{
    /**
     * @Route(
     *     name="api_add_profil",
     *     path="/api/profil/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.profil')->updateProfil($tab,'add');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_profil",
     *     path="/api/profil/edit",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.profil')->updateProfil($tab,'edit');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_profil",
     *     path="/api/profil/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.profil')->deleteProfil($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_profil",
     *     path="/api/profil/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
//        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.profil')->getAllProfil();
        $data = [];
        foreach ($res as $profil){
            $data[] =$profil->getData();
        }
        $view = $this->get('app.tools')->sendDataJson($data);
        return $view;
    }

    /**
     * @Route(
     *     name="api_one_profil",
     *     path="/api/profil/one",
     *     methods={"POST"}
     * )
     */
    public function findAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.profil')->getAllProfilById($tab);
        $view = $this->get('app.tools')->sendDataJson($res->getData());
        return $view;
    }


}
