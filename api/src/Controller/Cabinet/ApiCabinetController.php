<?php

namespace App\Controller\Cabinet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
class ApiCabinetController extends Controller
{
    /**
     * @Route(
     *     name="api_add_cabinet",
     *     path="/api/cabinet/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.cabinet')->addCabinet($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_cabinet",
     *     path="/api/cabinet/update",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.cabinet')->updateCabinet($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_cabinet",
     *     path="/api/cabinet/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.cabinet')->deleteCabinet($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_liste_cabinet",
     *     path="/api/cabinet/listing",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.cabinet')->listingCabinet($tab,$this->getUser());
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_data_cabinet",
     *     path="/api/cabinet/getdata",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.cabinet')->getDataCabinet($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
