<?php

namespace App\Controller\Bureau;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiBureauController extends Controller
{
    /**
     * @Route(
     *     name="api_add_office",
     *     path="/api/bureau/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->addBureau($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_office",
     *     path="/api/bureau/edit",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->editBureau($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_office",
     *     path="/api/bureau/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->deleteBureau($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_office",
     *     path="/api/bureau/all",
     *     methods={"POST"}
     * )
     */
    public function allAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->getAllBureau($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_one_office",
     *     path="/api/bureau/one",
     *     methods={"POST"}
     * )
     */
    public function getOneAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->getOneBureauById($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_data_office",
     *     path="/api/bureau/getdata",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.bureau')->getDataBureau($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
