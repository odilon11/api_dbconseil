<?php

namespace App\Controller\Entites;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiEntitesController extends Controller
{

    /**
     * @Route(
     *     name="api_add_entites",
     *     path="/api/entites/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->addEntites($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_entites",
     *     path="/api/entites/edit",
     *     methods={"POST"}
     * )
     */
    public function editAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->editEntites($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_entites",
     *     path="/api/entites/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->deleteEntites($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_entites",
     *     path="/api/entites/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->getAllEntites($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_data_entites",
     *     path="/api/entites/getdata",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->getOneEntiteById($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
