<?php

namespace App\Controller;

use App\Entity\Api\ApiResponse;
use App\Entity\Constants\Constant;
use App\Manager\CollaboratorManager;
use App\Services\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Collaborator extends Controller
{

    public function __construct()
    {

    }

    /**
     * @Route(
     *     name="api_get_collab_all_",
     *     path="/api/collaborateurs/all",
     *     methods={"POST"}
     * )
     */
    public function getAllCollab(Request $request)
    {
        $res = $this->get(CollaboratorManager::SERVICE_NAME)->getAll();
        return $res;
    }

    /**
     * @Route(
     *     name="api_add_collab_",
     *     path="/api/collaborateurs/add",
     *     methods={"POST"}
     * )
     */
    public function addCollab(Request $request)
    {

        $res = $this->get(CollaboratorManager::SERVICE_NAME)->add($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_edit_collab_",
     *     path="/api/collaborateurs/edit",
     *     methods={"POST"}
     * )
     */
    public function editCollab(Request $request)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->add($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_delete_collab_",
     *     path="/api/collaborators/delete/{id}",
     *     methods={"DELETE"}
     * )
     */
    public function deleteColab(Request $request, $id)
    {
        $res = $this->get(CollaboratorManager::SERVICE_NAME)->deleteColab($this->getUser(), $id);
        return $res;
    }

}
