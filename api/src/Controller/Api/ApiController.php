<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    /**
     * @Route(
     *     name="api_test",
     *     path="/api/test",
     *     methods={"POST"}
     * )
     */
    public function testAction(Request $request)
    {

        $tab = $this->get('app.tools')->getDataJson($request);

        $res = [
            'nom'=>$tab['nom'],
            'prenom'=>$tab['prenom']
        ];

        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;

    }

    /**
     * @Route(
     *     name="get_token",
     *     path="/getToken",
     *     methods={"POST"}
     * )
     */
    public function getToken(Request $request)
    {
        if($request->getContent()) {
            $data = json_decode($request->getContent());
            $userName = $data->username;
            $password = $data->password;
        }else{
            $userName = $request->get("username");
            $password = $request->get("password");
        }
        $userByName = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $userName]);
        $userByEmail = $this->getDoctrine()->getRepository('App:User')->findOneBy(['email' => $userName]);
        $user = ($userByEmail)?$userByEmail:$userByName;
        if (!$user) {
            return new JsonResponse($this->get('app.tools')->getStateSession(401,'User not found.'));
        }
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);
        if (!$isValid) {
            return new JsonResponse($this->get('app.tools')->getStateSession(401,'Password not valid.'));
        }
        $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
        return new JsonResponse(['token' => $jwtManager->create($user)]);
    }

    /**
     * @Route(
     *     name="reset",
     *     path="/reset",
     *     methods={"POST"}
     * )
     */
    public function resetAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->resetPwd($tab);
        return new JsonResponse($res);
      //  $view = $this->get('app.tools')->sendResponseJson($res);
       // return $view;
    }



}
