<?php

namespace App\Controller;

use App\Entity\Api\ApiResponse;
use App\Entity\Constants\Constant;
use App\Entity\User;
use App\Manager\EmailAutomatiqueManager;
use App\Services\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Login extends Controller
{

    public function __construct()
    {

    }

    /**
     * @Route(
     *     name="get_token",
     *     path="/getToken",
     *     methods={"POST"}
     * )
     */
    public function getToken(Request $request)
    {
        $response = new ApiRequest();
        $userName = $response->getBodyRawParam("username");
        $password = $response->getBodyRawParam("password");
        $userByName = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $userName]);
        $userByEmail = $this->getDoctrine()->getRepository('App:User')->findOneBy(['email' => $userName]);
        $user = ($userByEmail)?$userByEmail:$userByName;
        if (!$user) {
            return new JsonResponse(['error' => "user not found"]);
        }
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);
        if (!$isValid) {
            return new JsonResponse(['error' => "Password not valid."]);
        }
        $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
        return new JsonResponse(['token' => $jwtManager->create($user)]);
    }

    /**
     * @Route(
     *     name="api_send_reset_user",
     *     path="/reset/password/send-mail",
     *     methods={"POST"},
     * )
     */
    public function resetPasswordRequest(Request $request)
    {
        $resp = new ApiResponse();
        try {
            $apiRequest = new ApiRequest();
        } catch (\Exception $e) {
            return new JsonResponse($resp->setMessage($e->getMessage())
                ->setCode(Response::HTTP_BAD_REQUEST));
        }
        $email = $apiRequest->getBodyRawParam('email');

        if (!$email) {
            return new JsonResponse($resp->setCode(Response::HTTP_BAD_REQUEST)->setMessage('Missing parameters.'));
        }
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);
        if (null === $user) {
            return new JsonResponse($resp->setCode(Response::HTTP_NO_CONTENT)->setMessage('User not found.'));
        }
        $token = rtrim(strtr(base64_encode(random_bytes(64)), '+/', '-_'), '=');
        $user->setConfirmationToken($token);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);
        $resp->setData(["token" => $user->getConfirmationToken()]);
        $this->sendMailReset($email, $user->getConfirmationToken());
        return new JsonResponse($resp);
    }

    /**
     * Reset password
     *
     * @Route(
     *     methods={"POST"},
     *     path="/profile/api-password-reset/{token}",
     *     name="modify_password"
     * )
     */
    public function resetAction(Request $request, $token)
    {
        if($request->get('password')) {
            $password = $request->get('password');
        }else {
            $password = $request->query->get('password');
        }
        if (!$password) {
            return new JsonResponse(['error' => Response::HTTP_CONTINUE,'message' => 'Password not valid.']);
        }
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user) {
            return new JsonResponse(
                [
                    'error' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message' => 'The user with confirmation token does not exist for value =' . $token
                ]
            );
        }
        if ($request->getMethod() == "POST") {
            $user->setPlainPassword($password);
            $user->setConfirmationToken(null);
            $userManager->updateUser($user, true);
            return new JsonResponse(
                [
                    "code" => Response::HTTP_OK,
                    "message" => "Password resetting"
                ]
            );
        }
    }


    /**
     * @param $email
     * @param $token
     */
    public function sendMailReset($email, $token)
    {
        $url = $this->container->getParameter("host_front")."/#/fr/reinitialiser/".$token;
        $modelEMail = $this->get(EmailAutomatiqueManager::SERVICE_NAME)->findBy(
            ['declenchement' => Constant::RESET_PASSWORD, 'deletedAt' => null],
            ['id' => 'DESC'], 1
        );
        $dataFrom['send_by'] = $modelEMail[0]->getEmitter();
        $template = $modelEMail[0]->getTemplate();
        $modele = ["__url__"];
        $real = [$url];
        $template = str_replace($modele, $real, $template);
        $this->container->get('app.mailer')->sendMailGrid("Resset password", $email, $template, $dataFrom);
    }

    /**
     * @Route(
     *     name="get_user___",
     *     path="/api/currentUser",
     *     methods={"GET"}
     * )
     */
    public function getUserAction()
    {
        $resp = new ApiResponse();
        $resp->setData($this->getUser()->getData());

        return new JsonResponse($resp, Response::HTTP_OK);
    }

}
