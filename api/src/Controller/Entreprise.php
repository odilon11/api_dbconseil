<?php

namespace App\Controller;

use App\Entity\Api\ApiResponse;
use App\Entity\Constants\Constant;
use App\Manager\CollaboratorManager;
use App\Services\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Entreprise extends Controller
{

    public function __construct()
    {

    }

    /**
     * @Route(
     *     name="api_get_all_",
     *     path="/api/entreprises_",
     *     methods={"POST"}
     * )
     */
    public function getAllByFilter(Request $request)
    {
        $res = $this->get(CollaboratorManager::SERVICE_NAME)->getAll();
        return $res;
    }

    /**
     * @Route(
     *     name="api_add_entreprise_",
     *     path="/api/entreprises/add",
     *     methods={"POST"}
     * )
     */
    public function add(Request $request)
    {

        $res = $this->get(CollaboratorManager::SERVICE_NAME)->add($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_edit_entreprise_",
     *     path="/api/entreprises/edit",
     *     methods={"POST"}
     * )
     */
    public function edit(Request $request)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->edit($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_delete_entreprise_",
     *     path="/api/entreprises/delete/{id}",
     *     methods={"DELETE"}
     * )
     */
    public function delete(Request $request, $id)
    {
        $res = $this->get(CollaboratorManager::SERVICE_NAME)->deleteCollab($this->getUser(), $id);
        return $res;
    }

}
