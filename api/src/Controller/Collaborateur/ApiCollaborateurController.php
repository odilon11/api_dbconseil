<?php

namespace App\Controller\Collaborateur;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ApiCollaborateurController extends Controller
{

    /**
     * @Route(
     *     name="api_add_collaborateur",
     *     path="/api/collaborateur/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->updateCollab($tab,'add');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_collaborateur",
     *     path="/api/collaborateur/edit",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->updateCollab($tab,'edit');
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_delete_collaborateur",
     *     path="/api/collaborateur/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->deleteCollab($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_all_collaborateur",
     *     path="/api/collaborateur/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->getAllCollab($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_find_collaborateur",
     *     path="/api/collaborateur/find",
     *     methods={"POST"}
     * )
     */
    public function findAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->getAllCollabByName($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_one_collaborateur",
     *     path="/api/collaborateur/one",
     *     methods={"POST"}
     * )
     */
    public function getOneAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->getOneCollabById($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
