<?php

namespace App\Controller\Employe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiEmployeController extends Controller
{

    /**
     * @Route(
     *     name="api_add_employe",
     *     path="/api/employe/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.employe')->addEmploye($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_edit_employe",
     *     path="/api/employe/edit",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.employe')->editEmploye($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_delete_employe",
     *     path="/api/employe/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.employe')->deleteEmploye($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_all_employe",
     *     path="/api/employe/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.employe')->getAllEmploye();
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_one_employe",
     *     path="/api/employe/one",
     *     methods={"POST"}
     * )
     */
    public function getOneAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.employe')->getOneEmployeById($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_find_employe",
     *     path="/api/employe/find",
     *     methods={"POST"}
     * )
     */
    public function findAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.collab')->getAllCollabByName($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }



    /**
     * @Route(
     *     name="api_data_employe",
     *     path="/api/employe/data",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.entites')->getDataEntites($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
