<?php

namespace App\Controller;

use App\Entity\Api\ApiResponse;
use App\Entity\Constants\Constant;
use App\Manager\OfficeManager;
use App\Services\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Office extends Controller
{

    public function __construct()
    {

    }

    /**
     * @Route(
     *     name="api_get_office_all",
     *     path="/api/bureau/all",
     *     methods={"POST"}
     * )
     */
    public function getAllOffice(Request $request)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->getAll();
        return $res;
    }

    /**
     * @Route(
     *     name="api_add_office_",
     *     path="/api/bureaux/add",
     *     methods={"POST"}
     * )
     */
    public function addOffice(Request $request)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->add($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_edit_offices",
     *     path="/api/bureaux/edit",
     *     methods={"POST"}
     * )
     */
    public function editOffice(Request $request)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->edit($this->getUser());
        return $res;
    }

    /**
     * @Route(
     *     name="api_delete_office_",
     *     path="/api/bureaux/delete/{id}",
     *     methods={"DELETE"}
     * )
     */
    public function deleteOffice(Request $request, $id)
    {
        $res = $this->get(OfficeManager::SERVICE_NAME)->deleteOffice($this->getUser(), $id);
        return $res;
    }

}
