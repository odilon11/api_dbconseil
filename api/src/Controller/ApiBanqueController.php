<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiBanqueController extends Controller
{
    /**
     * @Route(
     *     name="api_add_bank",
     *     path="/api/banque/add",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.banque')->addBanque($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_edit_bank",
     *     path="/api/banque/edit",
     *     methods={"POST"}
     * )
     */
    public function editAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.banque')->editBanque($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_delete_bank",
     *     path="/api/banque/delete",
     *     methods={"POST"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.banque')->deleteBanque($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_all_bank",
     *     path="/api/banque/all",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.banque')->getAllBanque($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_one_bank",
     *     path="/api/banque/one",
     *     methods={"POST"}
     * )
     */
    public function getDataAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.banque')->getOneBanque($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

}
