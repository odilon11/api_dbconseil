<?php

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiUserController extends Controller
{
    /**
     * @Route(
     *     name="api_add_user",
     *     path="/api/user/create",
     *     methods={"POST"}
     * )
     */
    public function createAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->getUtilisateur($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }


    /**
     * @Route(
     *     name="api_valid_mdp_user",
     *     path="/api/user/valid-mdp",
     *     methods={"POST"}
     * )
     */
    public function validMdpAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->validMdpByUser($tab);
        $view = $this->get('app.tools')->sendDataJson($res);

        return $view;
    }



    /**
     * @Route(
     *     name="api_valid_user",
     *     path="/api/user/valid",
     *     methods={"POST"}
     * )
     */
    public function validgAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->validUser($tab);
        $view = $this->get('app.tools')->sendDataJson($res);

        return $view;
    }

    /**
     * @Route(
     *     name="api_confirmation_user",
     *     path="/api/user/confirmation",
     *     methods={"POST"}
     * )
     */
    public function tokenAction(Request $request)
    {
        //$tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->setConfirmationToken();
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }





    /**
     * @Route(
     *     name="api_edit_user",
     *     path="/api/user/update",
     *     methods={"POST"}
     * )
     */
    public function updateAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->miseAjourUser($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        return $view;
    }

    /**
     * @Route(
     *     name="api_liste_user",
     *     path="/api/user/listing",
     *     methods={"POST"}
     * )
     */
    public function listingAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->listingUser($tab);
        $view = $this->get('app.tools')->sendDataJson($res);
        
        return $view;
    }

    /**
     * @Route(
     *     name="api_edit_expert_user",
     *     path="/api/user/update-expert",
     *     methods={"POST"}
     * )
     */
    public function updateExpertgAction(Request $request)
    {
        $tab = $this->get('app.tools')->getDataJson($request);
        $res = $this->get('app.utilisateur')->updateExpert($tab);
        $view = $this->get('app.tools')->sendDataJson($res);

        return $view;
    }

    /**
     * @Route(
     *     name="api_current_user",
     *     path="/api/user/current",
     *     methods={"POST"}
     * )
     */
    public function getCurrentUser(Request $request)
    {
       var_dump('erere');die;
    }


}
