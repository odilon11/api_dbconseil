<?php

namespace App\Controller;

use App\Entity\Api\ApiResponse;
use App\Manager\UserManager;
use App\Services\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class User extends Controller
{

    /**
     * @Route(
     *     name="api_get_user",
     *     path="/api/currentUser",
     *     methods={"GET"}
     * )
     */
    public function getUserAction()
    {
        $resp = new ApiResponse();
        $resp->setData($this->getUser()->getData());

        return new JsonResponse($resp, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     name="api_get_user_by_id",
     *     path="/api/user/{id}",
     *     methods={"GET"}
     * )
     */
    public function getUserById($id)
    {
        $resp = new ApiResponse();
        if(!$user = $this->get(UserManager::SERVICE_NAME)->find($id)){
            return new JsonResponse($resp->setMessage("User not found"), Response::HTTP_NOT_FOUND);
        }
        $resp->setData($user->getData());

        return new JsonResponse($resp, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     name="api_get_all_user",
     *     path="/api/users",
     *     methods={"GET"}
     * )
     */
    public function getAllUser()
    {
        $resp = new ApiResponse();
        $users = $this->getDoctrine()->getManager()->getRepository("App:User")->findAll();
        $data = [];
        foreach ($users as $adata){
            $data[] = $adata->getData();
        }
        $resp->setData($data);

        return new JsonResponse($resp, Response::HTTP_OK);
    }
}
